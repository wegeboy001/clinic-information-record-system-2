<?php include('includes/header.php') ?>

<div class="container">
  <div class="row">
    <div class="col-md-6 mr-auto ml-auto text-center py-5 mt-5">
      <div class="card">
        <div class="card-body">
          <h1 class="card-title bg-danger text-white"> Error Page </h1>
            <h2 class="card-title"> 404 Error </h2>
            <p class="card-title"> The Page You are Searching is Not Available.</p>
            <a href="login.php" class="btn btn-primary"> Go back to Login Page</a>
          </div>
        </div>
      </div>
    </div>
  </div>
