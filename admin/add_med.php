<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>


      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="img/occ.jpg">
            <?php echo $_SESSION['username']; ?>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Clinic Information Record System </h1>
      </div>

  <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Add Medicine  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">
        <div class="modal-body">
          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-4">
                <label> Expire Date </label>
                <input type="date" value="<?php echo date('Y-m-d'); ?>" name="med_expire" class="form-control" placeholder="" required>
            </div>
            <div class="form-group col-md-4">
                <label> Generic Name </label>
                <input type="med_name" name="med_name" class="form-control checking_email" placeholder="" required>
                <small class="error_email" style="color: red;"></small>
            </div>
            </div>
            <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-4">
                <label>Status</label>
                <select id="inputState" class="form-control" name="med_status" placeholder="" required>
                  <option selected></option>
                  <option>Available</option>
                  <option>Not Available</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label>Brand Name</label>
                <select id="inputState" class="form-control" name="med_brand" placeholder="">
                  <option selected></option>
                  <option>None</option>
                  <option>Pfizer</option>
                  <option>UNILAB</option>
                  <option>PHILMED</option>
                  <option>Boehringer Ingelheim</option>
                </select>
            </div>
          </div>
          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-8">
              <label for="exampleFormControlTextarea1"> Description </label>
              <textarea class="form-control" type="med_description" name="med_description" id="exampleFormControlTextarea1" rows="3"></textarea>
          </div>
        </div>
          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-4">
                <label>Medicine Price</label>
                <input type="med_price" name="med_price" class="form-control" placeholder="" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="add_medbtn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Medicine Inventory</h6>
      <br>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
      Add Medicine
    </button>
    <a href="print_medicine.php" class="btn btn-success"><span>Print <i class="fa fa-print" aria-hidden="true"></i></span></a>
</div>
<div class="card-body">


  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      $query = "SELECT * FROM med_add";
      $query_run = mysqli_query($connection, $query);
     ?>
    <table border="3" bordercolor="grey" class="table table-hover" id="datatableid" width="100%" cellspacing="10">
      <thead>
        <tr class="table-danger">
          <th>#</th>
          <th>Edit / View</th>
          <th>Delete</th>
          <th>Expire Date</th>
          <th>Generic Name</th>
          <th> Price</th>
          <th>Status</th>
          <th>Brand Name</th>
          <th>Description</th>

        </tr>
      </thead>
      <tbody>

          <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?>
        <tr>
          <td class="table-dark"><?php echo $row['medicine_id'];?></td>
          <td class="table-success">
              <form action="edit_add_med.php" method="post">
                  <input type="hidden" name="addmed_id" value="<?php echo $row['medicine_id'];?>">
              <button type="submit" name="edit_addmed_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></i>&nbsp;&nbsp;<i class='fas fa-eye'></i></button>
              </form>
          </td>
          <td class="table-success">
              <form action="code.php" method="post">
              <input type="hidden" name="delete_med_id" value="<?php echo $row['medicine_id']; ?>">
              <button type="submit" name="delete_medicine_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
              </form>
          </td>

          <td class="table-success"><?php echo $row['expire_date'];?></td>
          <td class="table-success"><?php echo $row['medicine_name'];?></td>
          <td class="table-success"><?php echo $row['medicine_price'];?></td>
          <td class="table-success"><?php echo $row['medicine_status'];?></td>
          <td class="table-success"><?php echo $row['medicine_brand'];?></td>
          <td class="table-success"><?php echo $row['medicine_desc'];?></td>

        </tr>
          <?php
              }
           }
           else {
             echo "No Record Found";
           }
          ?>

      </tbody>
    </table>
  </div>
</div>
</div>

  </div>



</div>


<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->




  <?php
  include('includes/script.php');
  include('includes/footer.php');
   ?>
