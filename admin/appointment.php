<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');


$newArray = [];
$s_AppDate ="";
$s_PatientID=0;
$s_MP_ID=0;
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>


      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="img/occ.jpg">
            <?php echo $_SESSION['username']; ?>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Clinic Information Record System </h1>
      </div>

  <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Add Appointment  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">
        <div class="modal-body">
          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-8">
                <label> Patient Name </label>
                <!-- <input type="text" name="patient_name" class="form-control" placeholder="" required> -->
                <select id="inputState" class="form-control" name="patient_id" placeholder="" required>
                  <option selected value=""> Select Patient</option>
                  <?php
                  $sql = "SELECT * FROM patient_record";
                  if ($result = mysqli_query($connection,$sql)) {
                    if (mysqli_num_rows($result) > 0) {
                      while ($row = mysqli_fetch_array($result)) {
                      echo "<option value = ".$row['id'] .">".$row['name'] ."</option>";
                      }
                    }
                  }
                   ?>

                </select>
            </div>
            </div>
            <!-- <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-8s">
                <label>Appointment Date</label>
                <input type="date" name="app_date" class="form-control" placeholder="" required>
            </div>
          </div> -->
          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-8">
                <label>Appointment Date</label>
                <input type="date" name="app_date" value="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder="" required>
            </div>
        </div>
          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-8">
                <label>Appointment Time</label>
                <input type="time" name="app_time" class="form-control" placeholder="" required>
            </div>
        </div>
        <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-8">
            <label>Medical Practitioner</label>
            <select id="inputState" class="form-control" name="mp_id" placeholder="" required>
              <option selected value=""> Select Medical Practitioner</option>
              <?php
              $sql = "SELECT * FROM med_prac";
              if ($result = mysqli_query($connection,$sql)) {
                if (mysqli_num_rows($result) > 0) {
                  while ($row = mysqli_fetch_array($result)) {
                  echo "<option value = ".$row['mp_id'] .">".$row['mp_name'] ."</option>";
                  }
                }
              }
               ?>

            </select>
        </div>
      </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="add_appointment_btn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Appointment</h6>
      <br>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
      Book
    </button>
    <a href="print_appointment.php" class="btn btn-success"><span>Print <i class="fa fa-print" aria-hidden="true"></i></span></a>
</div>
<div class="card-body">


  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      // $query = "SELECT * FROM appointment as a join patient_record as b on a.patient_id = b.id join med_prac as c on a.mp_id = c.mp_id";
      // $query_run = mysqli_query($connection, $query);
     ?>

     <table>
       <form class="form-group col-md-4" action="code.php" method="post">
         <tr>
           <th><label>  Appointment Date</label></th>
           <th>
             <input type="date" name="app_date" class="form-control checking_email" value="<?php echo date('Y-m-d'); ?>">
           </th>
           <th>
             <select id="inputState" class="form-control" name="mp_id" placeholder="" >
               <option selected value=""> Select Medical Practitioner</option>
               <?php
               $sql = "SELECT * FROM med_prac";
               if ($result = mysqli_query($connection,$sql)) {
                 if (mysqli_num_rows($result) > 0) {
                   while ($row = mysqli_fetch_array($result)) {
                   echo "<option value = ".$row['mp_id'] .">".$row['mp_name'] ."</option>";
                   }
                 }
               }
                ?>
             </select>
           </th>
           <th>
             <select id="inputState" class="form-control" name="patient_id" placeholder="" >
               <option selected value=""> Select Patient Name</option>
               <?php
               $sql = "SELECT * FROM patient_record";
               if ($result = mysqli_query($connection,$sql)) {
                 if (mysqli_num_rows($result) > 0) {
                   while ($row = mysqli_fetch_array($result)) {
                   echo "<option value = ".$row['id'] .">".$row['name'] ."</option>";
                   }
                 }
               }
                ?>
             </select>
           </th>
           <th><input type="submit" class="btn btn-primary" name="searchAppointment" value="Search"></th>
         </tr>


       </form>
     </table>


    <!-- <table border="3" bordercolor="grey" class="table table-hover" id="datatableid" width="100%" cellspacing="10">
      <thead>
        <tr class="table-danger">
          <th>#</th>
          <th>Edit</th>
          <th>Delete</th>
          <th>Patient</th>
          <th>Appointment Date</th>
          <th>Appointment Time</th>
          <th>Medical Practitioner</th>


        </tr>
      </thead>
      <tbody> -->

          <!-- <?php
            //if(mysqli_num_rows($query_run) > 0)
            {
              //  while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?> -->
        <!-- <tr>
          <td class="table-dark"><?php echo $row['appointment_id'];?></td>
          <td class="table-success">
              <form action="edit_appointment.php" method="post">
                  <input type="hidden" name="appointment_id" value="<?php echo $row['appointment_id'];?>">
              <button type="submit" name="edit_appointment_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
              </form>
          </td>
          <td class="table-success">
              <form action="code.php" method="post">
              <input type="hidden" name="delete_app_id" value="<?php echo $row['appointment_id']; ?>">
              <button type="submit" name="delete_app_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
              </form>
          </td>
          <td class="table-success"><?php echo $row['name'];?></td>
          <td class="table-success"><?php echo $row['app_date'];?></td>
          <td class="table-success"><?php echo $row['app_time'];?></td>
          <td class="table-success"><?php echo $row['mp_name'];?></td>


        </tr>
          <?php
              }
           }
          // else {
          //   echo "No Record Found";
          // }
          ?>

      </tbody>
    </table> -->


    <!-- NEW TABLE -->
    <table border="3" bordercolor="grey" class="table table-hover" width="100%" cellspacing="10">
      <thead>
        <tr>
          <th>#</th>
          <th>Edit</th>
          <th>Delete</th>
          <th>Patient</th>
          <th>Appointment Date</th>
          <th>Appointment Time</th>
          <th>Medical Practitioner</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if (ISSET($_SESSION['search_appdate'])) {
          $s_AppDate = $_SESSION['search_appdate'];
        }
        else {
          $s_AppDate="";
        }

        if (ISSET($_SESSION['search_mp_id'])) {
          $s_MP_ID=(int)$_SESSION['search_mp_id'];
        }
        else {
          $s_MP_ID=0;
        }

        if (ISSET($_SESSION['search_patient_id'])) {
          $s_PatientID=(int)$_SESSION['search_patient_id'];
        }
        else {
          $s_PatientID=0;
        }

        //CONDITION PARA SA QUERY
        if ($s_AppDate<>"" AND $s_MP_ID<>0 AND $s_PatientID==0) {//IF Application Date and Med Prac is Selected
          $_SESSION['print_patient_id']=$s_PatientID;
          $_SESSION['print_mp_id']=$s_MP_ID;
          $_SESSION['print_appdate']=$s_AppDate;
          $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.mp_id=$s_MP_ID AND a.app_date='$s_AppDate' order by a.app_date DESC ";
          $query_runSelect = mysqli_query($connection, $sqlQuery);

        if(mysqli_num_rows($query_runSelect) > 0)
        {
           while ($row = mysqli_fetch_assoc($query_runSelect))
             {?>
               <tr>
               <td class="table-dark"><?php echo $row['appointment_id'];?></td>
               <td class="table-success">
                   <form action="edit_appointment.php" method="post">
                       <input type="hidden" name="appointment_id" value="<?php echo $row['appointment_id'];?>">
                   <button type="submit" name="edit_appointment_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
                   </form>
               </td>
               <td class="table-success">
                   <form action="code.php" method="post">
                   <input type="hidden" name="delete_app_id" value="<?php echo $row['appointment_id']; ?>">
                   <button type="submit" name="delete_app_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                   </form>
               </td>
               <td class="table-success"><?php echo $row['name'];?></td>
               <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
               <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
               <td class="table-success"><?php echo $row['mp_name'];?></td>
               </tr>
      <?php }

    }else {
      echo "No Record";

    }
  }//END SA FIRST CONDITION NGA APP DATE AND MED PRAC LNG ANG EH SELECT
  elseif ($s_AppDate<>"" AND $s_PatientID<>0 AND $s_MP_ID==0)
  {//IF Application Date and Patient is Selected
    $_SESSION['print_patient_id']=$s_PatientID;
    $_SESSION['print_mp_id']=$s_MP_ID;
    $_SESSION['print_appdate']=$s_AppDate;
          $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.patient_id=$s_PatientID AND a.app_date='$s_AppDate' order by a.app_date DESC ";
          $query_runSelect = mysqli_query($connection, $sqlQuery);

      if(mysqli_num_rows($query_runSelect) > 0)
      {
         while ($row = mysqli_fetch_assoc($query_runSelect))
           {?>
             <tr>
             <td class="table-dark"><?php echo $row['appointment_id'];?></td>
             <td class="table-success">
                 <form action="edit_appointment.php" method="post">
                     <input type="hidden" name="appointment_id" value="<?php echo $row['appointment_id'];?>">
                 <button type="submit" name="edit_appointment_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
                 </form>
             </td>
             <td class="table-success">
                 <form action="code.php" method="post">
                 <input type="hidden" name="delete_app_id" value="<?php echo $row['appointment_id']; ?>">
                 <button type="submit" name="delete_app_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                 </form>
             </td>
             <td class="table-success"><?php echo $row['name'];?></td>
             <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
             <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
             <td class="table-success"><?php echo $row['mp_name'];?></td>
             </tr>
    <?php }

    }else {
    echo "No Record";
    }

  }//END SA SECOND CONDITION NGA APP DATE AND PATIENT LNG ANG EH SELECT
  elseif ($s_AppDate<>"" AND $s_PatientID<>0 AND $s_MP_ID<>0)
  {//IF Application Date and Med Prac and Patient is Selected
    $_SESSION['print_patient_id']=$s_PatientID;
    $_SESSION['print_mp_id']=$s_MP_ID;
    $_SESSION['print_appdate']=$s_AppDate;
          $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.patient_id=$s_PatientID AND a.mp_id=$s_MP_ID AND a.app_date='$s_AppDate' order by a.app_date DESC ";
          $query_runSelect = mysqli_query($connection, $sqlQuery);

          if(mysqli_num_rows($query_runSelect) > 0)
          {
             while ($row = mysqli_fetch_assoc($query_runSelect))
               {?>
                 <tr>
                 <td class="table-dark"><?php echo $row['appointment_id'];?></td>
                 <td class="table-success">
                     <form action="edit_appointment.php" method="post">
                         <input type="hidden" name="appointment_id" value="<?php echo $row['appointment_id'];?>">
                     <button type="submit" name="edit_appointment_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
                     </form>
                 </td>
                 <td class="table-success">
                     <form action="code.php" method="post">
                     <input type="hidden" name="delete_app_id" value="<?php echo $row['appointment_id']; ?>">
                     <button type="submit" name="delete_app_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                     </form>
                 </td>
                 <td class="table-success"><?php echo $row['name'];?></td>
                 <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
                 <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
                 <td class="table-success"><?php echo $row['mp_name'];?></td>
                 </tr>
        <?php }

        }else {
        echo "No Record";
        }
  }//END SA THIRD CONDITION NGA APP DATE AND PATIENT UG MED PRAC ANG EH SELECT
  elseif ($s_AppDate<>"" AND $s_PatientID==0 AND $s_MP_ID==0)
  {//IF Application Date is Selected
    $_SESSION['print_patient_id']=$s_PatientID;
    $_SESSION['print_mp_id']=$s_MP_ID;
    $_SESSION['print_appdate']=$s_AppDate;
          $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.app_date='$s_AppDate' order by a.app_date DESC ";
          $query_runSelect = mysqli_query($connection, $sqlQuery);

          if(mysqli_num_rows($query_runSelect) > 0)
          {
             while ($row = mysqli_fetch_assoc($query_runSelect))
               {?>
                 <tr>
                 <td class="table-dark"><?php echo $row['appointment_id'];?></td>
                 <td class="table-success">
                     <form action="edit_appointment.php" method="post">
                         <input type="hidden" name="appointment_id" value="<?php echo $row['appointment_id'];?>">
                     <button type="submit" name="edit_appointment_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
                     </form>
                 </td>
                 <td class="table-success">
                     <form action="code.php" method="post">
                     <input type="hidden" name="delete_app_id" value="<?php echo $row['appointment_id']; ?>">
                     <button type="submit" name="delete_app_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                     </form>
                 </td>
                 <td class="table-success"><?php echo $row['name'];?></td>
                 <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
                 <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
                 <td class="table-success"><?php echo $row['mp_name'];?></td>
                 </tr>
        <?php }

        }else {
        echo "No Record";
        }
  }//END SA Fourth CONDITION NGA APP DATE ANG EH SELECT
  else{
    echo "No Record";

  }
  $_SESSION['search_patient_id']="";
  $_SESSION['search_mp_id']="";
  $_SESSION['search_appdate']="";

    ?>
      </tbody>
    </table>
    <!-- END NEW TABLE -->
  </div>
</div>
</div>

  </div>



</div>


<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->




  <?php
  include('includes/script.php');
  include('includes/footer.php');
   ?>
