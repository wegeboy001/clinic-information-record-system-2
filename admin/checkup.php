<?php

include('security.php');
include('includes/header.php');
include('includes/navbar.php');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>


      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="img/occ.jpg">
            <?php echo $_SESSION['username']; ?>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>


    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Clinic Information Record System </h1>

      </div>

  <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Full in Checkup Details </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">
        <div class="modal-body">

            <div class="form-group">
                <label> Patient Name </label>
                <select id="inputState" class="form-control" name="patient_id" placeholder="" required>
                  <option selected value=""> Select Patient Name</option>
                  <?php
                  $sql = "SELECT * FROM patient_record";
                  if ($result = mysqli_query($connection,$sql)) {
                    if (mysqli_num_rows($result) > 0) {
                      while ($row = mysqli_fetch_array($result)) {
                      echo "<option value = ".$row['id'] .">".$row['name'] ."</option>";
                      }
                    }
                  }
                   ?>

                </select>
                <!-- <input type="schoolid" name="school-id" class="form-control" placeholder="" required> -->

            </div>
            <!-- <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control checking_email" placeholder="" required>
                <small class="error_email" style="color: red;"></small>
            </div> -->
            <div class="form-group">
                <label>Med.Practitioner</label>
                <!-- <input type="text" name="med_prac" class="form-control" placeholder="" required> -->
                <select id="inputState" class="form-control" name="mp_id" placeholder="" required>
                  <option selected value=""> Select Medical Practitioner</option>
                  <?php
                  $sql = "SELECT * FROM med_prac";
                  if ($result = mysqli_query($connection,$sql)) {
                    if (mysqli_num_rows($result) > 0) {
                      while ($row = mysqli_fetch_array($result)) {
                      echo "<option value = ".$row['mp_id'] .">".$row['mp_name'] ."</option>";
                      }
                    }
                  }
                   ?>

                </select>
            </div>
            <div class="form-group">
                <label>Medication</label>
                <input type="text" name="findings" class="form-control" placeholder="" required>
            </div>
            <div class="form-group">
              <label>Medicine</label>
              <select id="inputState" class="form-control" name="med_id" placeholder="" >
                <option selected value=""> Select Medicine</option>
                <?php
                $sql = "SELECT * FROM med_add";
                if ($result = mysqli_query($connection,$sql)) {
                  if (mysqli_num_rows($result) > 0) {
                    while ($row = mysqli_fetch_array($result)) {
                    echo "<option value = ".$row['medicine_id'] .">".$row['medicine_name'] ."</option>";
                    }
                  }
                }
                 ?>

              </select>
            </div>
            <div class="form-group">
                <label>Med. Given</label>
                <input type="number" name="qty" class="form-control" placeholder="" >
            </div>
            <div class="form-group">
                <label>Date</label>
                <input type="date" name="date" value="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder="" required>
            </div>
            <input type="hidden" name="usertype" value="admin">


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="reg-checkup-btn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Checkup</h6>
      <br>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
      Create
    </button>
</div>
<div class="card-body">


  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      $query = "SELECT *,a.id as checkup_id from checkup as a join patient_record as b on a.patient_id =b.id left join med_add as c on c.medicine_id=a.medicine_id join med_prac as d on d.mp_id=a.mp_id";
      $query_run = mysqli_query($connection, $query);
     ?>
    <table border="3" bordercolor="grey" class="table table-hover" id="datatableid" width="100%" cellspacing="10">
      <thead>
        <tr class="table-danger">
          <th>#</th>
          <th>Edit / View</th>
          <th>Delete</th>
          <th>ID</th>
          <th>Name</th>
          <th>Med.Practitioner</th>
          <th>Medicine</th>
          <!-- <th>Med. Given</th>
          <th>Findings</th>
          <th>Date</th> -->
        </tr>
      </thead>
      <tbody>

          <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?>
        <tr>
          <td class="table-dark"><?php echo $row['checkup_id'];?></td>
          <td class="table-success">
              <form action="checkup-edit.php" method="post">
                  <input type="hidden" name="checkup-id" value="<?php echo $row['checkup_id'];?>">
              <button type="submit" name="checkup-view-btn" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;<i class='fas fa-eye'></i></button>
              </form>
          </td>
          <td class="table-success">
              <form action="code.php" method="post">
              <input type="hidden" name="checkup-delete-id" value="<?php echo $row['checkup_id']; ?>">
              <button type="submit" name="checkup-delete-btn" class="btn btn-danger btn-sm"><i class='fas fa-trash-alt'></i></button>
              </form>
          </td>

          <td class="table-success"><?php echo $row['school_id'];?></td>
          <td class="table-success"><?php echo $row['name'];?></td>
          <td class="table-success"><?php echo $row['mp_name'];?></td>
          <td class="table-success"><?php echo $row['medicine_name'];?></td>
          <!-- <td class="table-success"><?php echo $row['qty'];?></td>
          <td class="table-success"><?php echo $row['findings'];?></td>
          <td class="table-success"><?php echo $row['date'];?></td> -->
        </tr>
          <?php
              }
           }
           else {
             echo "No Record Found";
           }
          ?>

      </tbody>
    </table>
  </div>
</div>
</div>

  </div>



</div>


<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->


<?php
include('includes/script.php');
include('includes/footer.php');
 ?>
