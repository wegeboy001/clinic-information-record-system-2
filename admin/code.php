  <?php
session_start();
include('security.php');


$connection = mysqli_connect("localhost","root","","adminpanel");




#User Registration button
if(isset($_POST['registerbtn']))
{
    $username   = $_POST['username'];
    $email      = $_POST['email'];
    $password   = $_POST['password'];
    $cpassword  = $_POST['confirmpassword'];
    $usertype  = $_POST['usertype'];

    $email_query = "SELECT * FROM register WHERE email='$email' ";
    $email_query_run = mysqli_query($connection, $email_query);
    if(mysqli_num_rows($email_query_run) > 0)
    {
      $_SESSION['status'] = "Email already taken. Try another one.";
      $_SESSION['status_code'] = "error";
      header('Location: register.php');
    }
    else
    {
    if($password === $cpassword)
    {
      $query = "INSERT INTO register (username,email,password,usertype) VALUES ('$username','$email','$password', '$usertype')";
      $query_run = mysqli_query($connection, $query);

      if($query_run)
      {
        //echo "Saved";
        $_SESSION['status'] = "Successfully Added.!";
        $_SESSION['status_code'] = "success";
        header('Location: register.php');
      }else
      {
        $_SESSION['status'] = "Failed.!";
        $_SESSION['status_code'] = "error";
        header('Location: register.php');
      }
    }
    else {
      $_SESSION['status'] = "Password and Confirm Password did NOT match";
      $_SESSION['status_code'] = "warning";
      header('Location: register.php');
    }
  }
}


#User Registration Edit button
  if(isset($_POST['edit_btn']))
  {
    $id = $_POST['edit_id'];

    $query = "SELECT * FROM register WHERE id='$id' ";
    $query_run = mysqli_query($connection, $query);
  }




#User Registration Update button
if(isset($_POST['updatebtn']))
{
  $id         = $_POST['edit_id'];
  $username   = $_POST['edit_username'];
  $email      = $_POST['edit_email'];
  $password   = $_POST['edit_password'];
  $usertypeupdate = $_POST['update_usertype'];

  $query = "UPDATE register SET username='$username', email='$email',password='$password',usertype='$usertypeupdate' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "The Data has been Updated";
    $_SESSION['status_code'] = "success";
    header('Location: register.php');
  }
  else
  {
    $_SESSION['status'] = "The Data is not Updated";
    $_SESSION['status_code'] = "error";
    header('Location: register.php');
  }
}





#User Registration Delete button
if(isset($_POST['delete_btn']))
{
  $id = $_POST['delete_id'];

  $query = "DELETE FROM register WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted Successfully";
    $_SESSION['status_code'] = "success";
    header('Location: register.php');
  }
  else
  {
    $_SESSION['status'] = "Failed.!";
    $_SESSION['status_code'] = "error";
    header('Location: register.php');
  }
}




#login form
if(isset($_POST['login_btn']))
{
  $username_login      = $_POST['username'];
  $password_login      = $_POST['password'];

  $query = "SELECT * FROM register WHERE username='$username_login' AND password='$password_login' ";
  $query_run = mysqli_query($connection, $query);

  if(mysqli_fetch_array($query_run))
  {
    $_SESSION['username'] = $username_login;
    header('Location: index.php');
  }
  else {
    $_SESSION['status'] = 'Email or Password is Invalid';
    header('Location: login.php');
  }
}




#Create monthly and annual report
if(isset($_POST['createbtn']))
{
  $schoolyear         = $_POST['schoolyear'];
  $semester       = $_POST['semester'];
  $date             = $_POST['date'];
  $med_case         = $_POST['medical_cases'];
  $outbreak         = $_POST['outbreak'];

  $query = "INSERT INTO monthly_report (schoolyear,semester,date,medical_cases,outbreak)
  VALUES ('$schoolyear','$semester','$date','$med_case','$outbreak')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Report information added.";
    $_SESSION['status_code'] = "success";
    header('Location: monthly_annual_report.php');
  }else
  {
    $_SESSION['status'] = "Report information NOT added.";
    $_SESSION['status_code'] = "error";
    header('Location: monthly_annual_report.php');
  }
}

#Delete button of report form
if(isset($_POST['report_delete_btn']))
{
  $id = $_POST['report_delete_id'];

  $query = "DELETE FROM monthly_report WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted";
    $_SESSION['status_code'] = "success";
    header('Location: monthly_annual_report.php');
  }
  else
  {
    $_SESSION['status'] = "Date is Not Deleted";
    $_SESSION['status_code'] = "error";
    header('Location: monthly_annual_report.php');
  }
}


#Edit button of report form
if(isset($_POST['report_edit_btn']))
{
  $id = $_POST['report_edit_id'];

  $query = "SELECT * FROM monthly_report WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}


#Update button of report form
if(isset($_POST['report_update']))
{
  $id         = $_POST['report_edit_id'];
  $school_year   = $_POST['update_schoolyear'];
  $semester      = $_POST['update_semester'];
  $date         = $_POST['update_date'];
  $med_cases = $_POST['update_medical_cases'];
  $outbreak = $_POST['update_outbreak'];

  $query = "UPDATE monthly_report SET schoolyear='$school_year', semester='$semester',date='$date',medical_cases='$med_cases',outbreak='$outbreak' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "The Data has been Updated";
    $_SESSION['status_code'] = "success";
    header('Location: monthly_annual_report.php');
  }
  else
  {
    $_SESSION['status'] = "The Data is not Updated";
    $_SESSION['status_code'] = "error";
    header('Location: monthly_annual_report.php');
  }
}

#Medical HISTORY delete button
if(isset($_POST['history_delete_btn']))
{
  $id = $_POST['history_delete_id'];

  $query = "DELETE FROM medical_history WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted!";
    $_SESSION['status_code'] = "success";
    header('Location: patient_medical_history.php');
  }
  else
  {
    $_SESSION['status'] = "Failed";
    $_SESSION['status_code'] = "error";
    header('Location: patient_medical_history.php');
  }
}
#Medical History View button
if(isset($_POST['medicalhistory_btn']))
{
  $id = $_POST['medicalhistory_id'];

  $query = "SELECT * FROM medical_history WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}

#Patient Record Delete
if(isset($_POST['patientrecord_delete_btn']))
{
  $id = $_POST['patientrecord_id'];

  $query = "DELETE FROM patient_record WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted!";
    $_SESSION['status_code'] = "success";
    header('Location: patient_record.php');
  }
  else
  {
    $_SESSION['status'] = "Failed";
    $_SESSION['status_code'] = "error";
    header('Location: patient_record.php');
  }
}
#Patient Record EDIT
if(isset($_POST['patientrecord_edit_btn']))
{
  $id = $_POST['patientedit_id'];

  $query = "SELECT * FROM patient_record WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}
#Patient Record View
if(isset($_POST['patientrecord_view_btn']))
{
  $id = $_POST['patientview_id'];

  $query = "SELECT * FROM patient_record WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}
// if(isset($_POST['checking_viewbtn']))
// {
//   $id = $_POST['student_id'];
//   // echo $return = $s_id;
//
//   $query = "SELECT * FROM patient_record WHERE id='$id' ";
//   $query_run = mysqli_query($connection, $query);
//
//   if(mysqli_num_rows($query_run) > 0)
//   {
//     foreach($query_run as $row)
//     {
//       echo $return = '
//           <h5> school_id:  '.$row['school_id'].'</h5>
//           <h5> name:  '.$row['name'].'</h5>
//           <h5> phone:  '.$row['phone'].'</h5>
//           <h5> description:  '.$row['description'].'</h5>
//           <h5> date:  '.$row['date'].'</h5>
//           <h5> status:  '.$row['status'].'</h5>
//           <h5> age:  '.$row['age'].'</h5>
//           <h5> city:  '.$row['city'].'</h5>
//           <h5> province:  '.$row['province'].'</h5>
//           <h5> zipcode:  '.$row['zipcode'].'</h5>
//       ';
//     }
//   }
//   else
//   {
//     echo $return = "<h5> No record found</h5>";
//   }
// }
#Patient Record UPDATE button
if(isset($_POST['newpatient_update']))
{
  $id          = $_POST['newpatient_id'];
  $schoolid   = $_POST['patient_id'];
  $name        = $_POST['patient_name'];
  $phone       = $_POST['patient_phone'];
  $desc       = $_POST['patient_description'];
  $Gender     = $_POST['patient_gender'];
  $bday       = $_POST['patient_birthday'];
  $status     = $_POST['patient_status'];
  $age        = $_POST['patient_age'];
  $city       = $_POST['patient_city'];
  $province   = $_POST['patient_province'];
  $zip        = $_POST['patient_zip'];
  $Address    = $_POST['patient_address'];

  $query = "UPDATE patient_record SET school_id='$schoolid', name='$name', phone='$phone', description='$desc', gender='$Gender',
  date='$bday', status='$status', age='$age', city='$city', province='$province', zipcode='$zip', address='$Address' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: patient_record.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: patient_record.php');
  }
}

if(isset($_POST['savebtn']))
{
  $School_ID       = $_POST['patient_id'];
  $name            = $_POST['patient_name'];
  $Phone_No        = $_POST['patient_phone'];
  $Description     = $_POST['patient_description'];
  $Gender          = $_POST['patient_gender'];
  $Birthday        = $_POST['patient_birthday'];
  $Status          = $_POST['patient_status'];
  $Age             = $_POST['patient_age'];
  $City            = $_POST['patient_city'];
  $Province        = $_POST['patient_province'];
  $Zip             = $_POST['patient_zip'];
  $Address         = $_POST['patient_address'];

  $query = "INSERT INTO patient_record (school_id,name,phone,description,gender,date,status,age,city,province,zipcode,address)
  VALUES ('$School_ID','$name','$Phone_No', '$Description','$Gender','$Birthday','$Status','$Age','$City','$Province','$Zip','$Address')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Patient Record Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: patient_record.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: patient_record.php');
  }
}

#Presctiption Save button

if(isset($_POST['save_prescription']))
{
  $School_ID       = $_POST['id'];
  $name            = $_POST['name'];
  $gender          = $_POST['gender'];
  $age             = $_POST['age'];
  $symptoms        = $_POST['symptoms'];
  $diagnosis       = $_POST['diagnosis'];
  $Medication      = $_POST['medications'];
  $Instruction     = $_POST['instructions'];


  $query = "INSERT INTO doctor_prescription (school_id,name,gender,age,symptoms,diagnosis,medications,instructions)
  VALUES ('$School_ID','$name','$gender', '$age','$symptoms','$diagnosis','$Medication','$Instruction')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: treatment.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: treatment.php');
  }
}

#Prescription EDIT button
if(isset($_POST['treatment_edit_btn']))
{
  $id = $_POST['treatment_id'];

  $query = "SELECT * FROM patient_record WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}

#Prescription DELETE button
if(isset($_POST['Prescription_delete']))
{
  $id = $_POST['Prescription_id'];

  $query = "DELETE FROM doctor_prescription WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted!";
    $_SESSION['status_code'] = "success";
    header('Location: treatment.php');
  }
  else
  {
    $_SESSION['status'] = "Failed";
    $_SESSION['status_code'] = "error";
    header('Location: treatment.php');
  }
}

#Prescription UPDATE button
if(isset($_POST['prescription_update_btn']))
{
  $id             = $_POST['prescription_id'];
  $schoolid       = $_POST['pres_id'];
  $name           = $_POST['pres_name'];
  $gender         = $_POST['pres_gender'];
  $age            = $_POST['pres_age'];
  $symptoms       = $_POST['pres_symptom'];
  $diagnosis      = $_POST['pres_diagnosis'];
  $medication     = $_POST['pres_medication'];
  $instruction    = $_POST['pres_instruction'];


  $query = "UPDATE doctor_prescription SET school_id='$schoolid', name='$name', gender='$gender', age='$age', symptoms='$symptoms', diagnosis='$diagnosis',
  medications='$medication', instructions='$instruction' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: treatment.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: treatment.php');
  }
}

//Checkup Save button
if(isset($_POST['reg-checkup-btn']))
{

  $patient_id        = $_POST['patient_id'];
  // $name            = $_POST['name'];
  $medprac         = $_POST['mp_id'];
  $findings        = $_POST['findings'];
  $date            = $_POST['date'];
  $med_id          = $_POST['med_id'];
  $qty            = $_POST['qty'];
if ($qty > 0) {
  $nqty = $qty * -1;
  $query1 = "INSERT INTO medicine_transaction (patient_id,mp_id,date,medicine_id,qty,trans_type)
  VALUES ('$patient_id','$medprac','$date','$med_id','$nqty', 'RELEASED')";
  $query_run = mysqli_query($connection, $query1);
}

  $query = "INSERT INTO checkup (patient_id,mp_id,findings,date,medicine_id,qty)
  VALUES ('$patient_id','$medprac', '$findings','$date','$med_id','$qty')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: checkup.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: checkup.php');
  }
}

// #Checkup View button
// if(isset($_POST['checkup-view-btn']))
// {
//   $id = $_POST['checkup-id'];
//
//   $query = "SELECT * FROM checkup WHERE id='$id' ";
//   $query_run = mysqli_query($connection, $query);
// }


#Checkup Delete button
if(isset($_POST['checkup-delete-btn']))
{
  $id = $_POST['checkup-delete-id'];

  $query = "DELETE FROM checkup WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted!";
    $_SESSION['status_code'] = "success";
    header('Location: checkup.php');
  }
  else
  {
    $_SESSION['status'] = "Failed";
    $_SESSION['status_code'] = "error";
    header('Location: checkup.php');
  }
}

//Checkup update button
$oldqty = 0;
$newqty = 0;
$totalqty = 0;
if(isset($_POST['checkup-update-btn']))
{
  $checkup_id    = $_POST['checkup-id'];
  $patient_id    = $_POST['patient_id'];
  $mp_id         = $_POST['mp_id'];
  $findings      = $_POST['findings'];
  $medicine_id   = $_POST['medicine_id'];
  $date          = $_POST['date'];
  $medqty        = $_POST['qty'];


//Get old qty in order to minus the new updated quantity
  if ($medqty>0) {
    //GET OLD qty
    $sqlGetOldQty = "SELECT * FROM checkup WHERE id='$checkup_id' ";
    if ($resultGetOldQty = mysqli_query($connection,$sqlGetOldQty)) {
      if (mysqli_num_rows($resultGetOldQty) > 0) {
        while ($rowsGetOldQty = mysqli_fetch_array($resultGetOldQty)) {
          $oldqty= $rowsGetOldQty['qty'];
        }
      }
    }

    if ($oldqty>$medqty) {
      $newqty = $oldqty + $medqty;
      $queryInsertInventory = "INSERT INTO medicine_transaction (medicine_id,date,qty,patient_id,mp_id,trans_type) VALUES ('$medicine_id','$date','$newqty','$patient_id','$mp_id','CHECKUP EDIT')";
      mysqli_query($connection, $queryInsertInventory);

    }elseif ($oldqty<$medqty) {
      $newqty = $medqty-$oldqty;
      $totalqty = $newqty * -1;
      //$newqty = gmp_neg("1");
      $queryInsertInventory = "INSERT INTO medicine_transaction (medicine_id,date,qty,patient_id,mp_id,trans_type) VALUES ('$medicine_id','$date',$totalqty,'$patient_id','$mp_id','CHECKUP EDIT RELEASED')";
      mysqli_query($connection, $queryInsertInventory);
    }


  }

  //UPDATE CHECKUP
  $query = "UPDATE checkup SET patient_id='$patient_id', mp_id='$mp_id', findings='$findings', date='$date',medicine_id ='$medicine_id',qty='$medqty' WHERE id='$checkup_id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: checkup.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: checkup.php');
  }
}

//Medicine Invenetory

if(isset($_POST['add_medbtn']))
{
    $date       = $_POST['med_expire'];
    $medname     = $_POST['med_name'];
    $medstatus   = $_POST['med_status'];
    $medbrand    = $_POST['med_brand'];
    $meddesc     = $_POST['med_description'];
    $medprice    = $_POST['med_price'];



    $query = "INSERT INTO med_add (expire_date,medicine_name,medicine_price,medicine_status,medicine_brand,medicine_desc)
    VALUES ('$date','$medname', '$medprice','$medstatus','$medbrand', '$meddesc')";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
      //echo "Saved";
      $_SESSION['status'] = "Saved!";
      $_SESSION['status_code'] = "success";
      header('Location: add_med.php');
    }else
    {
      $_SESSION['status'] = "Failed to Save!";
      $_SESSION['status_code'] = "error";
      header('Location: add_med.php');
    }
  }


// Stock in
  if(isset($_POST['savebtn_medinventory']))
  {
      $medid       = $_POST['medicine_id'];
      $qty         = $_POST['qty'];
      $mp_id       = $_POST['mp_id'];
      $patient_id  = $_POST['patient_id'];
      $date        = $_POST['date'];
      $expdate     = $_POST['exp_date'];



      $query = "INSERT INTO medicine_transaction (medicine_id,qty,mp_id,patient_id,date,med_expdate,trans_type)
      VALUES ('$medid','$qty', '$mp_id','$patient_id','$date','$expdate', 'STOCK IN')";
      $query_run = mysqli_query($connection, $query);

      if($query_run)
      {
        //echo "Saved";
        $_SESSION['status'] = "Saved!";
        $_SESSION['status_code'] = "success";
        header('Location: med_inventory.php');
      }else
      {
        $_SESSION['status'] = "Failed to Save!";
        $_SESSION['status_code'] = "error";
        header('Location: med_inventory.php');
      }
    }

 $expdate = "";
    // Stock Out
      if(isset($_POST['savebtn_medinventory_stockout']))
      {
          $medidtrans       = $_POST['medicine_id'];
          $qty         = $_POST['qty'] * -1;
          $mp_id       = $_POST['mp_id'];
          $patient_id  = $_POST['patient_id'];
          $date        = $_POST['date'];

          $query_getmed = "SELECT * FROM medicine_transaction as a WHERE a.id = $medidtrans ";
          if ($result_getmed = mysqli_query($connection,$query_getmed)) {
            if (mysqli_num_rows($result_getmed) > 0) {
              while ($rows_getmed = mysqli_fetch_array($result_getmed)) {
                $expdate=$rows_getmed['med_expdate'];
                $medid=$rows_getmed['medicine_id'];
              }
            }
          }

          $query = "INSERT INTO medicine_transaction (medicine_id,qty,mp_id,patient_id,date,med_expdate,trans_type)
          VALUES ('$medid','$qty', '$mp_id','$patient_id','$date','$expdate', 'RELEASED')";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            //echo "Saved";
            $_SESSION['status'] = "Saved!";
            $_SESSION['status_code'] = "success";
            header('Location: med_inventory_stockout.php');
          }else
          {
            $_SESSION['status'] = "Failed to Save!";
            $_SESSION['status_code'] = "error";
            header('Location: med_inventory_stockout.php');
          }
        }


        //Add Medicine EDIT
        $medicine_id = 0;
        $medicine_number = "";
        $medicine_name = "";
        $medicine_price = 0;
        $medicine_status = "";
        $medicine_brand ="";
        $medicine_desc ="";

        if(isset($_POST['med_add_update_btn']))
        {
          $id = $_POST['med_id'];


          $date           = $_POST['med_expire'];
          $medicine_name  = $_POST['med_name'];
          $medicine_price = $_POST['med_price'];
          $medicine_status = $_POST['med_status'];
          $medicine_brand =$_POST['med_brand'];
          $medicine_desc =$_POST['med_desc'];

          $query = "UPDATE med_add SET expire_date='$date', medicine_name='$medicine_name',medicine_price='$medicine_price',medicine_status='$medicine_status',medicine_brand='$medicine_brand',medicine_desc='$medicine_desc' WHERE medicine_id= $id ";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            //echo "Saved";
            $_SESSION['status'] = "Saved!";
            $_SESSION['status_code'] = "success";
            header('Location: add_med.php');
          }else
          {
            $_SESSION['status'] = "Failed to Save!";
            $_SESSION['status_code'] = "error";
            header('Location: add_med.php');
          }
        }

        //Add Medicine DELETE

        if(isset($_POST['delete_medicine_btn']))
        {
          $id = $_POST['delete_med_id'];

          $query = "DELETE FROM med_add WHERE medicine_id='$id' ";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            $_SESSION['status'] = "Deleted Successfully";
            $_SESSION['status_code'] = "success";
            header('Location: add_med.php');
          }
          else
          {
            $_SESSION['status'] = "Failed.!";
            $_SESSION['status_code'] = "error";
            header('Location: add_med.php');
          }
        }

        //Stock In DELETE
        if(isset($_POST['edit_stockin_btn']))
        {
          $id = $_POST['stockin_id'];

          $query = "DELETE FROM medicine_transaction WHERE id='$id' ";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            $_SESSION['status'] = "Deleted Successfully";
            $_SESSION['status_code'] = "success";
            header('Location: add_med.php');
          }
          else
          {
            $_SESSION['status'] = "Failed.!";
            $_SESSION['status_code'] = "error";
            header('Location: add_med.php');
          }
        }

        // Appointment Insert

        if(isset($_POST['add_appointment_btn']))
        {

            $patient_id       = $_POST['patient_id'];
            $app_date         = $_POST['app_date'];
            $app_time         = $_POST['app_time'];
            $mp_id            = $_POST['mp_id'];




            $query = "INSERT INTO appointment (patient_id,app_date,app_time,mp_id)
            VALUES ('$patient_id','$app_date', '$app_time','$mp_id')";
            $query_run = mysqli_query($connection, $query);

            if($query_run)
            {
              //echo "Saved";
              $_SESSION['status'] = "Saved!";
              $_SESSION['status_code'] = "success";
              header('Location: appointment.php');
            }else
            {
              $_SESSION['status'] = "Failed to Save!";
              $_SESSION['status_code'] = "error";
              header('Location: appointment.php');
            }
          }

          //Appointment DELETE

          if(isset($_POST['delete_app_btn']))
          {
            $id = $_POST['delete_app_id'];

            $query = "DELETE FROM appointment WHERE appointment_id='$id' ";
            $query_run = mysqli_query($connection, $query);

            if($query_run)
            {
              $_SESSION['status'] = "Deleted Successfully";
              $_SESSION['status_code'] = "success";
              header('Location: appointment.php');
            }
            else
            {
              $_SESSION['status'] = "Failed.!";
              $_SESSION['status_code'] = "error";
              header('Location: appointment.php');
            }
          }

          //appointment EDIT
          $appointment_id = 0;
          $patient_id = 0;
          $app_date ="";
          $app_time = "";
          $mp_id = 0;
          $patient_name = "";
          $mp_name = "";

          if(isset($_POST['appointment_update_btn']))
          {
            $id = $_POST['appointment_id'];


            $patient_id = $_POST['patient_id'];
            $app_date = $_POST['app_date'];
            $app_time = $_POST['app_time'];
            $mp_id = $_POST['mp_id'];

            $query = "UPDATE appointment SET patient_id='$patient_id', app_date='$app_date',app_time='$app_time',mp_id='$mp_id' WHERE appointment_id = $id ";
            $query_run = mysqli_query($connection, $query);

            if($query_run)
            {
              //echo "Saved";
              $_SESSION['status'] = "Saved!";
              $_SESSION['status_code'] = "success";
              header('Location: appointment.php');
            }else
            {
              $_SESSION['status'] = "Failed to Save!";
              $_SESSION['status_code'] = "error";
              header('Location: appointment.php');
            }
          }
//Total Remaining medicine

if(isset($_POST['a'])){

  $medicine_id_trans = $_POST['medicine_id'];
  $query_getmed = "SELECT * FROM medicine_transaction as a WHERE a.id = $medicine_id_trans ";
  if ($result_getmed = mysqli_query($connection,$query_getmed)) {
    if (mysqli_num_rows($result_getmed) > 0) {
      while ($rows_getmed = mysqli_fetch_array($result_getmed)) {
        $medicine_id=$rows_getmed['medicine_id'];
        $expdate=$rows_getmed['med_expdate'];
        $_SESSION['exp_date']  =$rows_getmed['med_expdate'];
      }
    }
  }
  $query = "SELECt sum(qty) AS total_qty FROM medicine_transaction WHERE medicine_id = $medicine_id AND med_expdate = '$expdate'";
  $query_run = mysqli_query($connection, $query);

  if(mysqli_num_rows($query_run) > 0)
  {
      while ($row = mysqli_fetch_assoc($query_run))
       {
         $_SESSION['tot']  = $row['total_qty'];
      }
    }

    $_SESSION['medmed']=$medicine_id;
  if($query_run)
  {
    //echo "Saved";

    $_SESSION['status'] = "Success!";
    $_SESSION['status_code'] = "success";
    header('Location: med_inventory.php');
  }else
  {
    $_SESSION['status'] = "Failed to calculate";
    $_SESSION['status_code'] = "error";
    header('Location: med_inventory.php');
  }
}

//Search Application by Application Date and Medical Practitioner
if (isset($_POST['searchAppointment'])) {
  $appdate = $_POST['app_date'];
  $mpid = (int)$_POST['mp_id'];
  $patientid = (int)$_POST['patient_id'];



  // if ($appdate<>"" AND $mpid<>"" and $patientid=0) {//IF Application Date and Med Prac is Selected
  //   $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.mp_id=$mpid AND a.app_date='$appdate' order by a.app_date DESC ";
  //   $query_runSelect = mysqli_query($connection, $sqlQuery);
  //
  //   //IF SUCCESS ANG QUERY
  //   if($query_runSelect)
  //   {
  //     $_SESSION['status'] = "Success!";
  //     $_SESSION['status_code'] = "success";
  //     $_SESSION['search_appdate'] = $appdate;
  //     $_SESSION['search_mp_id'] = $mpid;
  //     $_SESSION['search_patient_id'] = $patientid;
  //     header('Location: appointment.php');
  //   }else
  //   {
  //     $_SESSION['status'] = "Failed to calculate";
  //     $_SESSION['status_code'] = "error";
  //     header('Location: appointment.php?error=1');
  //   }
  // }
  // elseif ($appdate<>"" AND $patientid<>"" and $mpid=0) {//IF Application Date and Patient is Selected
  //   $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.mp_id=$mpid AND a.patient_id='$patientid' order by a.app_date DESC ";
  //   $query_runSelect = mysqli_query($connection, $sqlQuery);
  //
  //   //IF SUCCESS ANG QUERY
  //   if($query_runSelect)
  //   {
  //     $_SESSION['status'] = "Success!";
  //     $_SESSION['status_code'] = "success";
  //     $_SESSION['search_appdate'] = $appdate;
  //     $_SESSION['search_mp_id'] = $mpid;
  //     $_SESSION['search_patient_id'] = $patientid;
  //     header('Location: appointment.php');
  //   }else
  //   {
  //     $_SESSION['status'] = "Failed to calculate";
  //     $_SESSION['status_code'] = "error";
  //     header('Location: appointment.php?error=1');
  //   }
  // }elseif ($appdate<>"" AND $patientid<>0 AND $mpid<>0) {//IF Application Date and Med Prac and Patient is Selected
  //   $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.mp_id=$mpid AND a.patient_id='$patientid' AND a.mp_id=$mipid order by a.app_date DESC ";
  //   $query_runSelect = mysqli_query($connection, $sqlQuery);
  //
  //   //IF SUCCESS ANG QUERY
  //   if($query_runSelect)
  //   {
      $_SESSION['status'] = "Success!";
      $_SESSION['status_code'] = "success";
      $_SESSION['search_appdate'] = $appdate;
      $_SESSION['search_mp_id'] = $mpid;
      $_SESSION['search_patient_id'] = $patientid;
      header('Location: appointment.php');
  //   }else
  //   {
  //     $_SESSION['status'] = "Failed to calculate";
  //     $_SESSION['status_code'] = "error";
  //     header('Location: appointment.php?error=1');
  //   }
  // }



}//END ISSET POST['searchAppointment']


#Medical HISTORY

if(isset($_POST['medicalhistorybtn']))
{
  $School_ID           = $_POST['history_id'];
  $name                = $_POST['history_name'];
  $Drugallergies       = $_POST['history_allergies'];
  $Healthproblem       = $_POST['history_problem'];
  $Otherillness        = $_POST['history_illness'];
  $Operation           = $_POST['history_operation'];
  $Unhealthyhabbit     = $_POST['history_habbit'];
  $Medication          = $_POST['history_medication'];
  $Otherinformation    = $_POST['history_information'];

  $query = "INSERT INTO medical_history (school_id,name,drug_allergies,health_problem,other_illness,operation,unhealthy_habbits,medications,other_information)
  VALUES ('$School_ID','$name','$Drugallergies','$Healthproblem','$Otherillness','$Operation','$Unhealthyhabbit','$Medication','$Otherinformation')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: patient_medical_history.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: patient_medical_history.php');
  }
}

#Meidcal History Update button
if(isset($_POST['medical_history_update']))
{
  $id                 = $_POST['medicalhistory_id'];
  $history_schoolid   = $_POST['history_id'];
  $name                = $_POST['history_name'];
  $Drugallergies       = $_POST['history_allergies'];
  $Healthproblem       = $_POST['history_problem'];
  $Otherillness        = $_POST['history_illness'];
  $Operation            = $_POST['history_operation'];
  $habbit               = $_POST['history_habbit'];
  $medication         = $_POST['history_medication'];
  $information        = $_POST['history_information'];

  $query = "UPDATE medical_history SET school_id='$history_schoolid', name='$name', drug_allergies='$Drugallergies', health_problem='$Healthproblem', other_illness='$Otherillness',
  operation='$Operation', unhealthy_habbits='$habbit', medications='$medication', other_information='$information' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: patient_medical_history.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: patient_medical_history.php');
  }
}
#Medical History View Record
if(isset($_POST['ptnhistory_view_btn']))
{
  $id = $_POST['ptnhistory_id'];

  $query = "SELECT * FROM medical_history WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}


?>
