<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>


      <div class="container-fluid">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>
              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>
                  <!-- Dropdown - User Information -->
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                      Settings
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                      Activity Log
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Logout
                    </a>
                  </div>
                </li>

              </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
              </div>

              <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Admin Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="code.php" method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label> Username </label>
                        <input type="text" name="username" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control checking_email" placeholder="Enter Email">
                        <small class="error_email" style="color: red;"></small>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
                </div>
              </form>

            </div>
          </div>
        </div>
        <div class="container-fluid">

        <!-- DataTables Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 front-weight-bold text primary">Edit Medicine Information</h6>
        </div>
        <div class="card-body">
<?php
$connection = mysqli_connect("localhost","root","","adminpanel");
// $checkup_id = 0;
$medicine_id = 0;
$medicine_number = "";
$medicine_name = "";
$medicine_price = 0;
$medicine_status = "";
$medicine_brand ="";
$medicine_desc ="";
// $date = "";
if(isset($_POST['edit_addmed_btn']))
{
    $id = $_POST['addmed_id'];

    $query = "SELECT * FROM med_add WHERE medicine_id = $id";


    // foreach($query_run as $row)
    // {
    if ($query_run = mysqli_query($connection, $query)) {
      // code...
      if(mysqli_num_rows($query_run) > 0)
      {
          while ($row = mysqli_fetch_assoc($query_run))
           {
             $medicine_id       = $row['medicine_id'];
             $date              = $row['expire_date'];
             $medicine_name     = $row['medicine_name'];
             $medicine_price    = $row['medicine_price'];
             $medicine_status    = $row['medicine_status'];
             $medicine_brand    = $row['medicine_brand'];
             $medicine_desc     = $row['medicine_desc'];
          }
        }
      }

  ?>


  <form action="code.php" method="POST">
    <div class="modal-body">
        <input class="form-control" type="hidden" name="med_id" value="<?php echo $medicine_id; ?>">

        <div class="form-group">
        <label>Expire Date</label>
        <input type="date" class="form-control" type="date" name="med_expire" value="<?php echo $date; ?>">
          </div>

        <div class="form-group">
        <label>Generic Name</label>
        <input class="form-control" type="text" name="med_name" value="<?php echo $medicine_name; ?>">
          </div>
          <div class="form-group">
              <label>Status</label>
              <select id="inputState" class="form-control" name="med_status" placeholder="" required>
                <option selected value="<?php echo $medicine_status; ?>"> <?php echo $medicine_status; ?></option>
                <option>Available</option>
                <option>Not Available</option>
              </select>
          </div>
          <div class="form-group">
              <label>Brand Name</label>
              <select id="inputState" class="form-control" name="med_brand" placeholder="" required>
                <option selected value="<?php echo $medicine_brand; ?>"> <?php echo $medicine_brand; ?></option>
                <option>Pfizer</option>
                <option>UNILAB</option>
              </select>
          </div>
        <div class="form-group">
        <label>Description</label>
        <input class="form-control" type="text" name="med_desc" value="<?php echo $medicine_desc; ?>">
          </div>
          <div class="form-group">
          <label>Medicine Price</label>
          <input class="form-control" type="text" name="med_price" value="<?php echo $medicine_price; ?>">
            </div>



    </div>
    <div class="modal-footer">
        <a href="add_med.php" class="btn btn-danger"> CANCEL </a>
        <button type="submit" name="med_add_update_btn" class="btn btn-primary">Update</button>
    </div>
  </form>

      <?php
    // }
  }
      ?>
</div>
</div>
</div>
  <!-- /.container fluid-->


  <?php
  include('includes/script.php');
  include('includes/footer.php');
   ?>
