<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="js/sweetalert.min.js"></script>
<?php
  if(isset($_SESSION['status']) && $_SESSION['status'] !='')
  {
    ?>
            <script>
            swal({
              title: "<?php echo $_SESSION['status']; ?>",
              //text: "You clicked the button!",
              icon: "<?php echo $_SESSION['status_code']; ?>",
              button: "Done!",
            });
            </script>
    <?php
    unset($_SESSION['status']);
  }
 ?>





<!-- Page level custom scripts -->
<script src="js/demo/chart-area-demo.js"></script>
<script src="js/demo/chart-pie-demo.js"></script>
<!-- Page level custom scripts -->
<script src="js/demo/datatables-demo.js"></script>

<script src="http://code.jquery.com/jquery-3.5.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script>

$(document).ready(function() {
    $('#datatableid').DataTable({
        "order":[[0,"desc"]],
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
        }
    });
});

</script>

<script>
  $(document).ready(function() {
    $('.deletebtn').on('click', function() {
      $('#deletemodal').modal('show');

        $tr = $(this).closest('tr');

        var data = $tr.children("td").map(function() {
          return $(this).text();
        }).get();

        console.log(data);

        $('#delete_id').val(data[0]);
    });
  });
</script>

<!-- <script>
    $(document).ready(function (){
      $('.view_btn').click(function (e) {
        e.preventDefault();

        //alert('Hello wege');
        var stud_id = $(this).closest('tr').find('.stud_id').text();
        // console.log(stud_id);
        //
        $.ajax({
            type: "POST",
            url: "code.php",
            data: {
                  'checking_viewbtn': true,
                  'student_id': stud_id,
            },
            dataType: "dataType",
            success: function (response) {
              // console.log(response);
              $('.student_viewing_data').html(response);
              $('#studentViewModal').modal('show');


            }
        });

    });
    });
</script> -->
