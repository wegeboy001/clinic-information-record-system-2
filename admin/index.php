<?php

include('security.php');
include('includes/header.php');
include('includes/navbar.php');
 ?>


 <!-- Content Wrapper -->
 <div id="content-wrapper" class="d-flex flex-column">

   <!-- Main Content -->
   <div id="content">

     <!-- Topbar -->
     <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

       <!-- Sidebar Toggle (Topbar) -->
       <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
         <i class="fa fa-bars"></i>
       </button>

       <!-- Topbar Search -->


       <!-- Topbar Navbar -->
       <ul class="navbar-nav ml-auto">

         <!-- Nav Item - Search Dropdown (Visible Only XS) -->
         <li class="nav-item dropdown no-arrow d-sm-none">
           <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <i class="fas fa-search fa-fw"></i>
           </a>
           <!-- Dropdown - Messages -->
           <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
             <form class="form-inline mr-auto w-100 navbar-search">
               <div class="input-group">
                 <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                 <div class="input-group-append">
                   <button class="btn btn-primary" type="button">
                     <i class="fas fa-search fa-sm"></i>
                   </button>
                 </div>
               </div>
             </form>
           </div>
         </li>



         <!-- Nav Item - User Information -->
         <li class="nav-item dropdown no-arrow">
           <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <img class="img-profile rounded-circle" src="img/occ.jpg">
             <?php echo $_SESSION['username']; ?>
           </a>
           <!-- Dropdown - User Information -->
           <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
             <a class="dropdown-item" href="#">
               <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
               Profile
             </a>
             <a class="dropdown-item" href="#">
               <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
               Settings
             </a>
             <a class="dropdown-item" href="#">
               <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
               Activity Log
             </a>
             <div class="dropdown-divider"></div>
             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
               <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
               Logout
             </a>
           </div>
         </li>

       </ul>

     </nav>
     <!-- End of Topbar -->

     <!-- Begin Page Content -->
     <div class="container-fluid">

       <!-- Page Heading -->
       <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
       </div>

       <!-- Content Row -->
       <div class="row">

         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-primary shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="register.php"><div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Registered User</div>
                     <br>
                   <div class="h5 mb-0 font-weight-bold text-gray-800">

                      <?php
                      $query = "SELECT id FROM register ORDER BY id";
                      $query_run = mysqli_query($connection, $query);

                      $row = mysqli_num_rows($query_run);

                      echo '<h4> User: '.$row.'</h4>';
                      ?>

                     </div>
                 </div></a>
                 <div class="col-auto">
                   <i class="fa fa-fw fa-user fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>

         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-success shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="patient_record.php"><div class="text-xs font-weight-bold text-success text-uppercase mb-1">Patient</div>
                     <br>
                   <div class="h5 mb-0 font-weight-bold text-gray-800">

                     <?php
                     $query = "SELECT id FROM patient_record ORDER BY id";
                     $query_run = mysqli_query($connection, $query);

                     $row = mysqli_num_rows($query_run);

                     echo '<h4> Record: '.$row.'</h4>';
                     ?>

                   </div></a>
                 </div>
                 <div class="col-auto">
                   <i class="fas fa-calendar fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>

         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-info shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="appointment.php"><div class="text-xs font-weight-bold text-info text-uppercase mb-1">Appointment</div>
                   <div class="row no-gutters align-items-center">
                     <div class="col-auto">
                       <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">

                         <?php
                         $query = "SELECT appointment_id FROM appointment ORDER BY appointment_id";
                         $query_run = mysqli_query($connection, $query);

                         $row = mysqli_num_rows($query_run);

                         echo '<h4> Booked:  '.$row.'</h4>';
                         ?>
                       </div></a>
                     </div>
                     <!-- <div class="col">
                       <div class="container">
                         <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                       </div>
                     </div> -->
                   </div>
                 </div>
                 <div class="col-auto">
                   <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>

         <!-- Pending Requests Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-warning shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="add_med.php"><div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Medicine</div>
                     <br>
                   <div class="h5 mb-0 font-weight-bold text-gray-800"></div>

                   <?php
                   $query = "SELECT medicine_id FROM med_add ORDER BY medicine_id";
                   $query_run = mysqli_query($connection, $query);

                   $row = mysqli_num_rows($query_run);

                   echo '<h4> Available: '.$row.'</h4>';
                   ?>

                 </div></a>
                 <div class="col-auto">
                   <i class="fas fa-file-prescription fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>

       <!-- Content Row -->
       <div class="container-fluid">

       <!-- DataTables Example -->
       <div class="card-body text-center">
         <img src="img/clinic.jpg" class="img-fluid" alt="Responsive image">
       </div>
     </div>


      </div>
      <!-- End of Main Content -->



<?php
include('includes/script.php');
include('includes/footer.php');

?>
