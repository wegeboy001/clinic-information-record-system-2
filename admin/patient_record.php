<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>



      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="img/occ.jpg">
            <?php echo $_SESSION['username']; ?>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Clinic Information Record System </h1>

      </div>
<!-- Add patient modal -->
      <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Fill patient information </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="code.php" method="POST">
            <div class="modal-body">
              <div class="form-row d-flex justify-content-center">
                <div class="form-group col-md-4">
                    <label> ID </label>
                    <input type="patient_id" name="patient_id" class="form-control" placeholder="Any valid ID" required>
                </div>
                <div class="form-group col-md-6">
                    <label> Name : (Last - First - Middle) </label>
                    <input type="text" name="patient_name" class="form-control checking_email" placeholder="" required>
                    <small class="error_email" style="color: red;"></small>
                </div>
                </div>
                <div class="form-row d-flex justify-content-center">
                  <div class="form-group col-md-6">
                    <label>Phone No. </label>
                    <input type="phone" name="patient_phone" class="form-control" placeholder="" required>
                </div>
                <div class="form-group col-md-2">
                    <label>Description</label>
                    <select id="inputState" class="form-control" name="patient_description" placeholder="" required>
                      <option selected></option>
                      <option>Student</option>
                      <option>Instructor</option>
                      <option>Employee</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label>Gender</label>
                    <select id="inputState" class="form-control" name="patient_gender" placeholder="" required>
                      <option selected></option>
                      <option>Male</option>
                      <option>Female</option>
                    </select>
                </div>
              </div>
              <div class="form-row d-flex justify-content-center">
                <div class="form-group col-md-6">
                    <label>Birthday</label>
                    <input type="date" name="patient_birthday" value="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder="" required>
                </div>
                <div class="form-group col-md-4">
                    <label>Status</label>
                    <select id="inputState" class="form-control" name="patient_status" placeholder="">
                      <option selected></option>
                      <option> 1st Year </option>
                      <option> 2nd Year </option>
                      <option> 3rd Year </option>
                      <option> 4th Year </option>
                    </select>
                </div>
                <!-- <div class="form-group col-md-2">
                    <label>Age</label>
                    <input type="age" name="patient_age" class="form-control" placeholder="" required>
                </div> -->
              </div>
              <div class="form-row d-flex justify-content-center">
                <div class="form-group col-md-4">
                    <label>City / Province</label>
                    <input type="city" name="patient_city" class="form-control" placeholder="" required>
                </div>
                <div class="form-group col-md-4">
                    <label>Municipality / Barangay</label>
                    <input type="province" name="patient_province" class="form-control" placeholder="" required>
                </div>
                <div class="form-group col-md-2">
                    <label>Zip</label>
                    <input type="zip" name="patient_zip" class="form-control" placeholder="" required>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="savebtn" class="btn btn-primary">Save</button>
            </div>
          </form>

        </div>
      </div>
    </div>


<!-- View patient info modal -->


<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> Patient Record</h6>
    <br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
    Register Patient
  </button>
</div>
<div class="card-body">


  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      $query = "SELECT * FROM patient_record";
      $query_run = mysqli_query($connection, $query);
     ?>
    <table border="3" bordercolor="grey" class="table table-hover" id="datatableid" width="100%" cellspacing="10">
      <thead>
        <tr align="center" class="table-danger">

          <th>#</th>
          <th>Edit</th>
          <th>Delete</th>
          <th>View</th>
          <th>ID</th>
          <th> Name</th>
          <th>Phone</th>
          <!-- <th>Description</th> -->
          <!-- <th>Gender</th>
          <th>Birthday</th>
          <th>Status</th>
          <th>Age</th>
          <th>City</th>
          <th>Province</th>
          <th>Zip </th>
          <th>Address</th> -->
        </tr>
      </thead>
      <tbody>

          <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?>
        <tr>

          <td class="table-dark"><?php echo $row['id'];?></td>
          <td class="table-success">
              <form action="patient_record_edit.php" method="post">
                  <input type="hidden" name="patientedit_id" value="<?php echo $row['id'];?>">
              <button type="submit" name="patientrecord_edit_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
              </form>
          </td>
          <td class="table-success">
              <form action="code.php" method="post">
              <input type="hidden" name="patientrecord_id" value="<?php echo $row['id']; ?>">
              <button type="submit" name="patientrecord_delete_btn" class="btn btn-danger btn-sm"><i class='fas fa-trash-alt'></i></button>
              </form>
          </td>
          <td class="stud_id table-success">
            <form action="view_patient_record.php" method="post">
            <input type="hidden" name="patientview_id" value="<?php echo $row['id']; ?>">
            <button type="submit" name="patientrecord_view_btn" class="btn btn-success btn-sm"><i class='fas fa-eye'></i>&nbsp;&nbsp;view</button>
            </form>
          </td>

          <td class="table-success"><?php echo $row['school_id'];?></td>
          <td class="table-success"><?php echo $row['name'];?></td>
          <td class="table-success"><?php echo $row['phone'];?></td>
          <!-- <td class="table-success"><?php echo $row['description'];?></td> -->
          <!-- <td class="table-success"><?php echo $row['gender'];?></td>
          <td class="table-success"><?php echo $row['date'];?></td>
          <td class="table-success"><?php echo $row['status'];?></td>
          <td class="table-success"><?php echo $row['age'];?></td>
          <td class="table-success"><?php echo $row['city'];?></td>
          <td class="table-success"><?php echo $row['province'];?></td>
          <td class="table-success"><?php echo $row['zipcode'];?></td>
          <td class="table-success"><?php echo $row['address'];?></td> -->
        </tr>
          <?php
              }
           }
           else {
             echo "No Record Found";
           }
          ?>

      </tbody>
    </table>
  </div>
</div>
</div>

  </div>

</div>
<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->


<?php
include('includes/script.php');
include('includes/footer.php');
 ?>
