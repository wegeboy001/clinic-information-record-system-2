<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>

      <div class="container-fluid">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>
              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>



                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="img-profile rounded-circle" src="img/occ.jpg">
                      <?php echo $_SESSION['username']; ?>
                  </a>
                  <!-- Dropdown - User Information -->
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                      Settings
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                      Activity Log
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Logout
                    </a>
                  </div>
                </li>

              </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
              </div>

        <div class="container-fluid">

        <!-- DataTables Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 front-weight-bold text primary"> Edit Information </h6>
        </div>
        <div class="card-body">
<?php
$connection = mysqli_connect("localhost","root","","adminpanel");
if(isset($_POST['patientrecord_edit_btn']))
{
    $id = $_POST['patientedit_id'];

    $query = "SELECT * FROM patient_record WHERE id='$id' ";
    $query_run = mysqli_query($connection, $query);

    foreach($query_run as $row)
  {
  ?>


  <form action="code.php" method="POST">
    <input type="hidden" name="newpatient_id" value="<?php echo $row['id'] ?>">
    <div class="modal-body">
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
            <label> ID </label>
            <input type="schoolid" name="patient_id" value="<?php echo $row['school_id'] ?> "class="form-control" placeholder="">
        </div>
        <div class="form-group col-md-6">
            <label>Name : (Last - First - Middle)</label>
            <input type="text" name="patient_name" value="<?php echo $row['name'] ?> "class="form-control" placeholder="">
            <small class="error_email" style="color: red;"></small>
        </div>
        </div>
        <div class="form-row d-flex justify-content-center">
          <div class="form-group col-md-4">
            <label>Phone No. </label>
            <input type="phone" name="patient_phone" value="<?php echo $row['phone'] ?> "class="form-control" placeholder="">
        </div>
        <div class="form-group col-md-4">
            <label>Description</label>
            <select id="inputState" class="form-control" name="patient_description" placeholder="" required>
              <option selected><?php echo $row['description'] ?></option>
              <option>Student</option>
              <option>Instructor</option>
              <option>Employee</option>
            </select>
        </div>
        <div class="form-group col-md-2">
            <label>Gender</label>
            <select id="inputState" class="form-control" name="patient_gender" placeholder="" required>
              <option selected><?php echo $row['gender'] ?></option>
              <option>Male</option>
              <option>Female</option>
            </select>
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-6">
            <label>Birthday</label>
            <input type="date" name="patient_birthday" value="<?php echo $row['date'] ?>" class="form-control" placeholder="" required>
        </div>
        <div class="form-group col-md-4">
            <label>Status</label>
            <select id="inputState" class="form-control" name="patient_status" placeholder="" required>
              <option selected><?php echo $row['status'] ?></option>
              <option> 1st Year </option>
              <option> 2nd Year </option>
              <option> 3rd Year </option>
              <option> 4th Year </option>
            </select>
        </div>
        <!-- <div class="form-group col-md-2">
            <label>Age</label>
            <input type="age" name="patient_age" value="<?php echo $row['age'] ?> "class="form-control" placeholder="">
        </div> -->
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
            <label>City / Province</label>
            <input type="city" name="patient_city" value="<?php echo $row['city'] ?> "class="form-control" placeholder="">
        </div>
        <div class="form-group col-md-4">
            <label>Municipality / Barangay</label>
            <input type="prov" name="patient_province" value="<?php echo $row['province'] ?> "class="form-control" placeholder="">
        </div>
        <div class="form-group col-md-2">
            <label>Zip</label>
            <input type="zip" name="patient_zip" value="<?php echo $row['zipcode'] ?> "class="form-control" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <!-- <div class="form-group col-md-6">
            <label>Address</label>
            <input type="add" name="patient_address" value="<?php echo $row['address'] ?> "class="form-control" placeholder="">
        </div> -->
      </div>
    </div>
    <div class="modal-footer">
      <a href="patient_record.php" class="btn btn-secondary"> CANCEL </a>
      <button type="submit" name="newpatient_update" class="btn btn-primary"> UPDATE </button>
    </div>
  </form>

      <?php
    }
  }
      ?>
</div>
</div>


</div>

  <!-- /.container fluid-->


<?php
include('includes/script.php');
include('includes/footer.php');
 ?>
