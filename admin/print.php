
<!DOCTYPE html>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title style="text-align:center;">Medical Form</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  </head>


  <body>

    <div class="container">
      <button onclick="printContent('div1')" class="fas fa-download fa-sm text-white-50"></button>
      <button type="button" class="btn btn-primary">Primary</button>

        <div class="formbody" id="div1">
          <div class="inner">
            <img src="img/occ.jpg" class="rounded mx-auto d-block" alt="Logo" width="150" height="150">
            <b><h1 style="text-align:center; color: black;" >OPOL COMMUNITY COLLEGE</h1></b>
            <b><h5 style="text-align:center; color: black;" >Poblacion, Opol, Mis. Or.</h5></b>
            <b><h5 style="text-align:center; color: black;">(088) 555 – 0518</h5></b>
            <b><h4 style="text-align:center; color: black;">Clinic Department Initial Report</h4></b>
            <br>
            <br>
            <b><p> Date: ________________ </p></b>
            <b><p> Time: ________________ </p></b>
            <b><p1>Department:
              <br>
            <b><p1>Total Number Of Cases:</p1></b>
            <br>
            <b><p1>Outbreak:
              <br>
              <br>
            <b><p1>Additional Notes:
              <br>
              <br>
              <br>
              <br>
              <b><p> ____________________________________ </p></b>
              <b><p> Signature Over Printed Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p></b>
          </div>
        </div>
    </div>
    <script>
            function printContent (el){
              var restorpage = document.body.innerHTML;
              var printcontent = document.getElementById(el).innerHTML;
              document.body.innerHTML = printcontent;
              window.print();
              document.body.innerHTML = restorpage;
            }

    </script>


  </body>
</html>
