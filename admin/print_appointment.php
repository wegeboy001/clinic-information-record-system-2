<?php
include('security.php');
include('includes/header.php');
$newArray = [];
$s_AppDate ="";
$s_PatientID=0;
$s_MP_ID=0;
 ?>

 <!DOCTYPE html>
 <html>
    <style type="text/css" media="print">
        @media print{
              .noprint, .noprint *{
                  display: none; !important;
              }
        }

    </style>

   <body onload="print()">
     <div class="container">

       <center>
            <img src="img/occ.jpg" style="width: 20%;" alt="">
            <h3 style="margin-top: 30px;"> Opol Community College</h3>
            <h3 style="margin-top: 10px;"> Clinic Department</h3>
            <h1 style="margin-top: 30px;"> Appointment Schedule</h1>


            <hr>

     </center>

     <table id="ready" class="table table-striped table-bordered" style="width: 100%;">
          <thead>
            <tr>

              <th>Patient</th>
              <th>Appointment Date</th>
              <th>Appointment Time</th>
              <th>Med. Practitioner</th>


            </tr>
          </thead>
          <tbody>
                <!-- <?php include 'database/dbconfig.php';
                      $get_stockin_report_list = mysqli_query($connection, "SELECT * FROM medicine_transaction as a join med_add as b on a.medicine_id = b.medicine_id join med_prac as c on c.mp_id = a.mp_id");

                      while($row = mysqli_fetch_array($get_stockin_report_list)){
                 ?>
                  <tr>
                    <td><?php echo $row['medicine_name'] ?></td>
                    <td><?php echo $row['date'] ?></td>
                    <td><?php echo $row['qty'] ?></td>
                    <td><?php echo $row['mp_name'] ?></td>
                    <td><?php echo $row['trans_type'] ?></td>
                  </tr>

               <?php } ?> -->


               <?php
               if (ISSET($_SESSION['print_appdate'])) {
                 $s_AppDate = $_SESSION['print_appdate'];
               }
               else {
                 $s_AppDate="";
               }

               if (ISSET($_SESSION['print_mp_id'])) {
                 $s_MP_ID=(int)$_SESSION['print_mp_id'];
               }
               else {
                 $s_MP_ID=0;
               }

               if (ISSET($_SESSION['print_patient_id'])) {
                 $s_PatientID=(int)$_SESSION['print_patient_id'];
               }
               else {
                 $s_PatientID=0;
               }

               //CONDITION PARA SA QUERY
               if ($s_AppDate<>"" AND $s_MP_ID<>0 AND $s_PatientID==0) {//IF Application Date and Med Prac is Selected
                 $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.mp_id=$s_MP_ID AND a.app_date='$s_AppDate' order by a.app_date DESC ";
                 $query_runSelect = mysqli_query($connection, $sqlQuery);

               if(mysqli_num_rows($query_runSelect) > 0)
               {
                  while ($row = mysqli_fetch_assoc($query_runSelect))
                    {?>
                      <tr>
                      <td class="table-success"><?php echo $row['name'];?></td>
                      <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
                      <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
                      <td class="table-success"><?php echo $row['mp_name'];?></td>
                      </tr>
             <?php }

           }else {
             echo "No Record";

           }
         }//END SA FIRST CONDITION NGA APP DATE AND MED PRAC LNG ANG EH SELECT
         elseif ($s_AppDate<>"" AND $s_PatientID<>0 AND $s_MP_ID==0)
         {//IF Application Date and Patient is Selected
                 $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.patient_id=$s_PatientID AND a.app_date='$s_AppDate' order by a.app_date DESC ";
                 $query_runSelect = mysqli_query($connection, $sqlQuery);

             if(mysqli_num_rows($query_runSelect) > 0)
             {
                while ($row = mysqli_fetch_assoc($query_runSelect))
                  {?>
                    <tr>
                    <td class="table-success"><?php echo $row['name'];?></td>
                    <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
                    <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
                    <td class="table-success"><?php echo $row['mp_name'];?></td>
                    </tr>
           <?php }

           }else {
           echo "No Record";
           }

         }//END SA SECOND CONDITION NGA APP DATE AND PATIENT LNG ANG EH SELECT
         elseif ($s_AppDate<>"" AND $s_PatientID<>0 AND $s_MP_ID<>0)
         {//IF Application Date and Med Prac and Patient is Selected
                 $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.patient_id=$s_PatientID AND a.mp_id=$s_MP_ID AND a.app_date='$s_AppDate' order by a.app_date DESC ";
                 $query_runSelect = mysqli_query($connection, $sqlQuery);

                 if(mysqli_num_rows($query_runSelect) > 0)
                 {
                    while ($row = mysqli_fetch_assoc($query_runSelect))
                      {?>
                        <tr>
                        <td class="table-success"><?php echo $row['name'];?></td>
                        <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
                        <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
                        <td class="table-success"><?php echo $row['mp_name'];?></td>
                        </tr>
               <?php }

               }else {
               echo "No Record";
               }
         }//END SA THIRD CONDITION NGA APP DATE AND PATIENT UG MED PRAC ANG EH SELECT
         elseif ($s_AppDate<>"" AND $s_PatientID==0 AND $s_MP_ID==0)
         {//IF Application Date is Selected
                 $sqlQuery="SELECT * FROM appointment as a left join med_prac as b on a.mp_id=b.mp_id left join patient_record as c on a.patient_id=c.id WHERE a.app_date='$s_AppDate' order by a.app_date DESC ";
                 $query_runSelect = mysqli_query($connection, $sqlQuery);

                 if(mysqli_num_rows($query_runSelect) > 0)
                 {
                    while ($row = mysqli_fetch_assoc($query_runSelect))
                      {?>
                        <tr>
                        <td class="table-success"><?php echo $row['name'];?></td>
                        <td class="table-success"><?php echo date('F d, Y',strtotime($row['app_date']));?></td>
                        <td class="table-success"><?php echo date("h:i:sa",strtotime($row['app_time']));?></td>
                        <td class="table-success"><?php echo $row['mp_name'];?></td>
                        </tr>
               <?php }

               }else {
               echo "No Record";
               }
         }//END SA Fourth CONDITION NGA APP DATE ANG EH SELECT
         else{
           echo "No Record";
           $_SESSION['print_patient_id']="";
           $_SESSION['print_mp_id']="";
           $_SESSION['print_appdate']="";
         }
         //CLEAR SESSION
         $_SESSION['search_patient_id']="";
         $_SESSION['search_mp_id']="";
         $_SESSION['search_appdate']="";

           ?>
          </tbody>

     </table>
     <br>
     <div class="container">
          <button type="" class="btn btn-info noprint" style="width 100%;" onclick="window.location.replace('appointment.php');">Cancel Printing</button>
     </div>

     </div>





   </body>
 </html>
