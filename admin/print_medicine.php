<?php
include('includes/header.php');
 ?>

 <!DOCTYPE html>
 <html>
    <style type="text/css" media="print">
        @media print{
              .noprint, .noprint *{
                  display: none; !important;
              }
        }

    </style>

   <body onload="print()">
     <div class="container">

       <center>
            <img src="img/occ.jpg" style="width: 20%;" alt="">
            <h3 style="margin-top: 30px;"> Opol Community College</h3>
            <h3 style="margin-top: 10px;"> Clinic Department</h3>
            <h1 style="margin-top: 30px;"> Medicine Inventory Report</h1>

            <hr>

     </center>

     <table id="ready" class="table table-striped table-bordered" style="width: 100%;">
          <thead>
            <tr>
              <th>Expire Date</th>
              <th>Generic Name</th>
              <th>Price</th>
              <th>Status</th>
              <th>Brand Name</th>
              <th>Description</th>


            </tr>
          </thead>
          <tbody>
                <?php include 'database/dbconfig.php';
                      $get_medicine_report_list = mysqli_query($connection, "SELECT * from med_add");

                      while($row = mysqli_fetch_array($get_medicine_report_list)){
                 ?>
                  <tr>
                    <td><?php echo $row['expire_date'] ?></td>
                    <td><?php echo $row['medicine_name'] ?></td>
                    <td><?php echo $row['medicine_price'] ?></td>
                    <td><?php echo $row['medicine_status'] ?></td>
                    <td><?php echo $row['medicine_brand'] ?></td>
                    <td><?php echo $row['medicine_desc'] ?></td>
                  </tr>

               <?php } ?>
          </tbody>

     </table>
     <br>
     <div class="container">
          <button type="" class="btn btn-info noprint" style="width 100%;" onclick="window.location.replace('add_med.php');">Cancel Printing</button>
     </div>

     </div>





   </body>
 </html>
