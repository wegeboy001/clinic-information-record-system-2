<?php
include('includes/header.php');
 ?>

 <!DOCTYPE html>
 <html>
    <style type="text/css" media="print">
        @media print{
              .noprint, .noprint *{
                  display: none; !important;
              }
        }

    </style>

   <body onload="print()">
     <div class="container">

       <center>
            <img src="img/occ.jpg" style="width: 20%;" alt="">
            <h3 style="margin-top: 30px;"> Opol Community College</h3>
            <h3 style="margin-top: 10px;"> Clinic Department</h3>
            <h1 style="margin-top: 30px;"> Monthly/Annual Report</h1>

            <hr>

     </center>

     <table id="ready" class="table table-striped table-bordered" style="width: 100%;">
          <thead>
            <tr>

              <th>School Year</th>
              <th>Semester</th>
              <th>Date</th>
              <th>Medical Cases</th>
              <th>Outbreak</th>


            </tr>
          </thead>
          <tbody>
                <?php include 'database/dbconfig.php';
                      $get_report_list = mysqli_query($connection, "SELECT * from monthly_report");

                      while($row = mysqli_fetch_array($get_report_list)){
                 ?>
                  <tr>
                    <td><?php echo $row['schoolyear'] ?></td>
                    <td><?php echo $row['semester'] ?></td>
                    <td><?php echo $row['date'] ?></td>
                    <td><?php echo $row['medical_cases'] ?></td>
                    <td><?php echo $row['outbreak'] ?></td>
                  </tr>

               <?php } ?>
          </tbody>

     </table>
     <br>
     <div class="container">
          <button type="" class="btn btn-info noprint" style="width 100%;" onclick="window.location.replace('monthly_annual_report.php');">Cancel Printing</button>
     </div>

     </div>





   </body>
 </html>
