<?php
include('includes/header.php');
 ?>

 <!DOCTYPE html>
 <html>
    <style type="text/css" media="print">
        @media print{
              .noprint, .noprint *{
                  display: none; !important;
              }
        }

    </style>

   <body onload="print()">
     <div class="container">

       <center>
            <img src="img/occ.jpg" style="width: 20%;" alt="">
            <h3 style="margin-top: 30px;"> Opol Community College</h3>
            <h3 style="margin-top: 10px;"> Clinic Department</h3>
            <h1 style="margin-top: 30px;"> Medicine Inventory Report</h1>
            <h5 style="margin-top: 10px;"> (Stock In)</h5>

            <hr>

     </center>

     <table id="ready" class="table table-striped table-bordered" style="width: 100%;">
          <thead>
            <tr>

              <th>Medicine</th>
              <th>Trans. Date</th>
              <th>Med. Qty</th>
              <th>Med. Practitioner</th>
              <th>Trans. Type</th>


            </tr>
          </thead>
          <tbody>
                <?php include 'database/dbconfig.php';
                      $get_stockin_report_list = mysqli_query($connection, "SELECT * FROM medicine_transaction as a join med_add as b on a.medicine_id = b.medicine_id join med_prac as c on c.mp_id = a.mp_id");

                      while($row = mysqli_fetch_array($get_stockin_report_list)){
                 ?>
                  <tr>
                    <td><?php echo $row['medicine_name'] ?></td>
                    <td><?php echo $row['date'] ?></td>
                    <td><?php echo $row['qty'] ?></td>
                    <td><?php echo $row['mp_name'] ?></td>
                    <td><?php echo $row['trans_type'] ?></td>
                  </tr>

               <?php } ?>
          </tbody>

     </table>
     <br>
     <div class="container">
          <button type="" class="btn btn-info noprint" style="width 100%;" onclick="window.location.replace('med_inventory.php');">Cancel Printing</button>
     </div>

     </div>





   </body>
 </html>
