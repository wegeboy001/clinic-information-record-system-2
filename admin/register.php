<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>


      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="img/occ.jpg">
            <?php echo $_SESSION['username']; ?>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>


    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Clinic Information Record System </h1>

      </div>

  <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Register User Account </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">
        <div class="modal-body">

            <div class="form-group">
                <label> Username </label>
                <input type="text" name="username" class="form-control" placeholder="Enter Username" required>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control checking_email" placeholder="Enter Email" required>
                <small class="error_email" style="color: red;"></small>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password" required>
            </div>

            <input type="hidden" name="usertype" value="admin">


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Admin</h6>
      <br>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
      Register Account
    </button>
</div>
<div class="card-body">


  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      $query = "SELECT * FROM register";
      $query_run = mysqli_query($connection, $query);
     ?>
    <table border="3" bordercolor="grey" class="table table-hover" id="datatableid" width="100%" cellspacing="10">
      <thead>
        <tr class="table-danger">
          <th>#</th>
          <th>Edit</th>
          <th>Delete</th>
          <th>View</th>
          <th>Username</th>
          <th>Email</th>
          <th>Password</th>
          <th>User.Type</th>
        </tr>
      </thead>
      <tbody>

          <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?>
        <tr>
          <td class="table-dark"><?php echo $row['id'];?></td>
          <td class="table-success">
              <form action="register_edit.php" method="post">
                  <input type="hidden" name="edit_id" value="<?php echo $row['id'];?>">
              <button type="submit" name="edit_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
              </form>
          </td>
          <td class="table-success">
            <form action="code.php" method="post">
               <input type="hidden" name="delete_id" value="<?php echo $row['id']; ?>">
               <button type="submit" name="delete_btn" class="btn btn-danger btn-sm"><i class='fas fa-trash-alt'></i></button>
           </form>
          </td>
          <td class="stud_id table-success">
            <form action="register_view.php" method="post">
            <input type="hidden" name="user_view_id" value="<?php echo $row['id']; ?>">
            <button type="submit" name="user_view_btn" class="btn btn-success btn-sm"><i class='fas fa-eye'></i>&nbsp;&nbsp;view</button>
            </form>
          </td>
          <td class="table-success"><?php echo $row['username'];?></td>
          <td class="table-success"><?php echo $row['email'];?></td>
          <td class="table-success"><?php echo $row['password'];?></td>
          <td class="table-success"><?php echo $row['usertype'];?></td>
        </tr>
          <?php
              }
           }
           else {
             echo "No Record Found";
           }
          ?>

      </tbody>
    </table>
  </div>
</div>
</div>

  </div>



</div>


<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->


<?php
include('includes/script.php');
include('includes/footer.php');
 ?>
