<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>

      <div class="container-fluid">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>

              <!-- Topbar Search -->
              <!-- <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form> -->

              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>



                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="img-profile rounded-circle" src="img/occ.jpg">
                      <?php echo $_SESSION['username']; ?>
                  </a>
                  <!-- Dropdown - User Information -->
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                      Settings
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                      Activity Log
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Logout
                    </a>
                  </div>
                </li>

              </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
              </div>

        <div class="container-fluid">

        <!-- DataTables Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 front-weight-bold text primary"> Edit Report </h6>
        </div>
        <div class="card-body">
<?php
$connection = mysqli_connect("localhost","root","","adminpanel");
if(isset($_POST['report_edit_btn']))
{
    $id = $_POST['report_edit_id'];

    $query = "SELECT * FROM monthly_report WHERE id='$id' ";
    $query_run = mysqli_query($connection, $query);

    foreach($query_run as $row)
  {
  ?>


    <form action="code.php" method="POST">
    <input type="hidden" name="report_edit_id" value="<?php echo $row['id'] ?>">
          <div class="form-group">
              <label> School Year (eg. 2020-2021) </label>
              <input type="schoolyear" name="update_schoolyear" value="<?php echo $row['schoolyear'] ?> "class="form-control" placeholder="Enter Username">
          </div>
          <div class="form-group">
              <label> Semester (eg. First/Second Semester) </label>
              <input type="semester" name="update_semester" value="<?php echo $row['semester'] ?> "class="form-control" placeholder="">

          </div>
          <div class="form-group">
              <label> Date </label>
              <input class="form-control" type="date" name="update_date" class="form-control" value="<?php echo $row['date'] ?>">
          </div>
          <div class="form-group">
              <label> Medical Cases </label>
              <input type="medicalcases" name="update_medical_cases" value="<?php echo $row['medical_cases'] ?> "class="form-control" placeholder="Enter Username">
          </div>
          <div class="form-group">
              <label> Outbreak </label>
              <input type="outbreak" name="update_outbreak" value="<?php echo $row['outbreak'] ?> "class="form-control" placeholder="Enter Username">
          </div>
          <a href="monthly_annual_report.php" class="btn btn-danger"> CANCEL </a>
          <button type="submit" name="report_update" class="btn btn-primary"> UPDATE </button>
        </form>

      <?php
    }
  }
      ?>
</div>
</div>
</div>
  <!-- /.container fluid-->


<?php
include('includes/script.php');
include('includes/footer.php');
 ?>
