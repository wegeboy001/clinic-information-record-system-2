<?php
include('security.php');

include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars"></i>
        </button>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

          <!-- Nav Item - Search Dropdown (Visible Only XS) -->
          <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
              <form class="form-inline mr-auto w-100 navbar-search">
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </li>



          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

              <img class="img-profile rounded-circle" src="img/occ.jpg">
              <?php echo $_SESSION['username']; ?>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="#">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profile
              </a>
              <a class="dropdown-item" href="#">
                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                Settings
              </a>
              <a class="dropdown-item" href="#">
                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                Activity Log
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Logout
              </a>
            </div>
          </li>

        </ul>

      </nav>
      <!-- End of Topbar -->

      <!-- Begin Page Content -->
      <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
        </div>

        <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">View Medical History Detail</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="code.php" method="POST">
          <div class="modal-body">

              <div class="form-group">
                  <label> Username </label>
                  <input type="text" name="username" class="form-control" placeholder="Enter Username">
              </div>
              <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" class="form-control checking_email" placeholder="Enter Email">
                  <small class="error_email" style="color: red;"></small>
              </div>
              <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Enter Password">
              </div>
              <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" name="registerbtn" class="btn btn-danger">Update</button>
          </div>
        </form>

      </div>
    </div>
  </div>

  <div class="container-fluid">

  <!-- DataTables Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 front-weight-bold text primary">View Medical History Detail</h6>
  </div>
  <div class="card-body">
<?php
$connection = mysqli_connect("localhost","root","","adminpanel");
if(isset($_POST['ptnhistory_view_btn']))
{
$id = $_POST['ptnhistory_id'];

$query = "SELECT * FROM medical_history WHERE id='$id' ";
$query_run = mysqli_query($connection, $query);

foreach($query_run as $row)
{
?>


<form action="code.php" method="POST">
  <input type="hidden" name="medicalhistory_id" value="<?php echo $row['id'] ?>">
  <div class="modal-body">
    <div class="form-row d-flex justify-content-center">
      <div class="form-group col-md-6">
          <label>  ID </label>
          <input type="schoolid" name="history_id" value="<?php echo $row['school_id'] ?> "class="form-control" placeholder="">
      </div>
      <div class="form-group col-md-6">
          <label> Name : (Last - First - Middle)</label>
          <input type="name" name="history_name" value="<?php echo $row['name'] ?> "class="form-control" placeholder="">
          <small class="error_email" style="color: red;"></small>
      </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-6">
            <label for="exampleFormControlTextarea1"> Drug Allergy </label>
            <textarea class="form-control" type="text" name="history_allergies" rows="3"><?php echo $row['drug_allergies']?></textarea>
        </div>
      <div class="form-group col-md-6">
          <label for="exampleFormControlTextarea1"> Medical History </label>
          <textarea class="form-control" type="text" name="history_problem" rows="3"><?php echo $row['health_problem']?></textarea>
      </div>
    </div>
    <div class="form-row d-flex justify-content-center">
      <!-- <div class="form-group col-md-6">
          <label for="exampleFormControlTextarea1"> Other Illness </label>
          <textarea class="form-control" type="text" name="history_illness" rows="3"><?php echo $row['other_illness']?></textarea>
      </div> -->
      <div class="form-group col-md-6">
          <label for="exampleFormControlTextarea1"> Operation </label>
          <textarea class="form-control" type="text" name="history_operation" rows="3"><?php echo $row['operation']?></textarea>
      </div>
      <div class="form-group col-md-6">
          <label for="exampleFormControlTextarea1"> Medications </label>
          <textarea class="form-control" type="text" name="history_medication" rows="3"><?php echo $row['medications']?></textarea>
      </div>
    </div>
    <div class="form-row d-flex justify-content-center">
      <!-- <div class="form-group col-md-6">
          <label for="exampleFormControlTextarea1"> Unhealthy Habbit </label>
          <textarea class="form-control" type="text" name="history_habbit" rows="3"><?php echo $row['unhealthy_habbits']?></textarea>
      </div> -->

    </div>
    <div class="form-row d-flex justify-content-center">
    <div class="form-group col-md-6">
        <label for="exampleFormControlTextarea1"> Other Information </label>
        <textarea class="form-control" type="text" name="history_information" rows="3"><?php echo $row['other_information']?></textarea>
    </div>
  </div>
  </div>
  <div class="modal-footer">
    <a href="patient_medical_history.php" class="btn btn-secondary"> Close </a>
  </div>
</form>

<?php
}
}
?>
</div>
<!-- <div class="form-row d-flex justify-content-center">
  <a href="patient_record.php" class="btn btn-primary"><i class="fas fa-arrow-left"></i>&nbsp; Back </a>
</div> -->
</div>
</div>
<!-- /.container fluid-->


<?php
include('includes/script.php');
include('includes/footer.php');
 ?>
