<?php


include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
?>


      <div class="container-fluid">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>
              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>
                  <!-- Dropdown - User Information -->
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                      Settings
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                      Activity Log
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Logout
                    </a>
                  </div>
                </li>

              </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
              </div>

              <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Admin Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="code.php" method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label> Username </label>
                        <input type="text" name="username" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control checking_email" placeholder="Enter Email">
                        <small class="error_email" style="color: red;"></small>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
                </div>
              </form>

            </div>
          </div>
        </div>
        <div class="container-fluid">

        <!-- DataTables Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 front-weight-bold text primary">Edit Appointment </h6>
        </div>
        <div class="card-body">
<?php
$connection = mysqli_connect("localhost","root","","adminpanel");
// $checkup_id = 0;
$appointment_id = 0;
$patient_id = 0;
$app_date ="";
$app_time = "";
$mp_id = 0;
$patient_name = "";
$mp_name = "";
// $date = "";
if(isset($_POST['edit_appointment_btn']))
{
    $id = $_POST['appointment_id'];

    $query = "SELECT * FROM appointment as a join patient_record as b on a.patient_id = b.id join med_prac as c on a.mp_id = c.mp_id WHERE a.appointment_id = $id";


    // foreach($query_run as $row)
    // {
    if ($query_run = mysqli_query($connection, $query)) {
      // code...
      if(mysqli_num_rows($query_run) > 0)
      {
          while ($row = mysqli_fetch_assoc($query_run))
           {
             $appointment_id = $row['appointment_id'];
             $patient_id =$row['patient_id'];
             $app_date =$row['app_date'];
             $app_time = $row['app_time'];
             $mp_id = $row['mp_id'];
             $patient_name = $row['name'];
             $mp_name = $row['mp_name'];
          }
        }
      }

  ?>


  <form action="user_code.php" method="POST">
    <div class="modal-body">
        <input class="form-control" type="hidden" name="appointment_id" value="<?php echo $appointment_id ; ?>">

        <div class="form-group">
          <label> Patient Name </label>
          <!-- <input type="text" name="patient_name" class="form-control" placeholder="" required> -->
          <select id="inputState" class="form-control" name="patient_id" placeholder="" required>
            <option value = "<?php echo $patient_id; ?>"><?php echo $patient_name; ?></option>
            <?php
            $sql = "SELECT * FROM patient_record";
            if ($result = mysqli_query($connection,$sql)) {
              if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
                echo "<option value = ".$row['id'] .">".$row['name'] ."</option>";
                }
              }
            }
             ?>

          </select>
          </div>



          <div class="form-group">
            <label>Appointment Date</label>
            <input class="form-control" type="date" name="app_date" class="form-control" value="<?php echo $app_date ; ?>">
            </div>
            <div class="form-group">
              <label>Appointment Time</label>
              <input class="form-control" type="time" name="app_time" class="form-control" value="<?php echo $app_time ; ?>">
              </div>
              <div class="form-group">
                <label> Medical Practitioner Name </label>
                <!-- <input type="text" name="patient_name" class="form-control" placeholder="" required> -->
                <select id="inputState" class="form-control" name="mp_id" placeholder="" required>
                  <option value = "<?php echo $mp_id; ?>"><?php echo $mp_name; ?></option>
                  <?php
                  $sql = "SELECT * FROM med_prac";
                  if ($result = mysqli_query($connection,$sql)) {
                    if (mysqli_num_rows($result) > 0) {
                      while ($row = mysqli_fetch_array($result)) {
                      echo "<option value = ".$row['mp_id'] .">".$row['mp_name'] ."</option>";
                      }
                    }
                  }
                   ?>

                </select>
                </div>



    </div>
    <div class="modal-footer">
        <a href="staff_appointment.php" class="btn btn-danger"> CANCEL </a>
        <button type="submit" name="appointment_update_btn" class="btn btn-primary">Update</button>
    </div>
  </form>

      <?php
    // }
  }
      ?>
</div>
</div>
</div>
  <!-- /.container fluid-->


  <?php
  include('staff_includes/user_script.php');
  include('staff_includes/user_footer.php');
   ?>
