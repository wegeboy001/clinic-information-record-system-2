<?php
session_start();
include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>


      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="img/staff.jpg">
              <h6>Patient</h6>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Clinic Information Record System </h1>
      </div>

  <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Create Patient Medical History </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="user_code.php" method="POST">
        <div class="modal-body">

          <div class="form-row d-flex justify-content-center">
            <div class="form-group col-md-4">
                <label>  ID </label>
                <input type="historyid" name="history_id" class="form-control" placeholder="Any valid ID" required>
            </div>
            <div class="form-group col-md-6">
                <label> Name : (Last - First - Middle) </label>
                <input type="text" name="history_name" class="form-control checking_email" placeholder="" required>
                <small class="error_email" style="color: red;"></small>
            </div>
            </div>
            <div class="form-row d-flex justify-content-center">
              <div class="form-group col-md-6">
                  <label for="exampleFormControlTextarea1"> Drug Allergies </label>
                  <textarea class="form-control" type="text" name="history_allergies" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
            <div class="form-group col-md-6">
                <label for="exampleFormControlTextarea1"> Medical History </label>
                <textarea class="form-control" type="text" name="history_problem" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
          <div class="form-row d-flex justify-content-center">
            <!-- <div class="form-group col-md-6">
                <label for="exampleFormControlTextarea1"> Other Illness </label>
                <textarea class="form-control" type="text" name="history_illness" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div> -->
            <div class="form-group col-md-6">
                <label for="exampleFormControlTextarea1"> Operation </label>
                <textarea class="form-control" type="text" name="history_operation" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="exampleFormControlTextarea1"> Medication </label>
                <textarea class="form-control" type="text" name="history_medication" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
          <div class="form-row d-flex justify-content-center">
            <!-- <div class="form-group col-md-6">
                <label for="exampleFormControlTextarea1"> Unhealthy Habbit </label>
                <textarea class="form-control" type="text" name="history_habbit" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div> -->

          </div>
          <div class="form-row d-flex justify-content-center">
          <div class="form-group col-md-10">
              <label for="exampleFormControlTextarea1"> Other Information </label>
              <textarea class="form-control" type="text" name="history_information" id="exampleFormControlTextarea1" rows="3"></textarea>
          </div>
        </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="medicalhistorybtn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> History</h6>
      <br>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">
      Create
    </button>
</div>
<div class="card-body">



  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      $query = "SELECT * FROM medical_history";
      $query_run = mysqli_query($connection, $query);
     ?>
    <table border="3" bordercolor="grey" class="table table-hover" id="tableid" width="100%" cellspacing="10">
      <thead>
        <tr class="table-danger">
          <th>#</th>
          <th>View</th>
          <th>ID</th>
          <th>Name</th>
          <!-- <th>Drug.Allergies</th> -->
          <th>Medical History</th>
          <!-- <th>Operation</th>
          <th>Medications</th> -->
          <th>Other.Information</th>
        </tr>
      </thead>
      <tbody>

          <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?>
        <tr>
          <td class="table-dark"><?php echo $row['id'];?></td>
          <!-- <td class="table-success">
              <form action="medical_history_edit.php" method="post">
                  <input type="hidden" name="medicalhistory_id" value="<?php echo $row['id'];?>">
              <button type="submit" name="medicalhistory_btn"class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button>
              </form>
          </td> -->
          <!-- <td class="table-success">
              <form action="user_code.php" method="post">
              <input type="hidden" name="medicalhistory_delete_id" value="<?php echo $row['id']; ?>">
              <button type="submit" name="medicalhistory_delete_btn" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
              </form>
          </td> -->
          <td class="stud_id table-success">
            <form action="medicalhistory_view.php" method="post">
            <input type="hidden" name="medicalhistory_view_id" value="<?php echo $row['id']; ?>">
            <button type="submit" name="medicalhistory_view_btn" class="btn btn-success btn-sm"><i class='fas fa-eye'></i>&nbsp;&nbsp;view</button>
            </form>
          </td>
          <td class="table-success"><?php echo $row['school_id'];?></td>
          <td class="table-success"><?php echo $row['name'];?></td>
          <td class="table-success"><?php echo $row['health_problem'];?></td>
          <!-- <td class="table-success"><?php echo $row['other_illness'];?></td> -->
          <!-- <td class="table-success"><?php echo $row['medications'];?></td>
          <td class="table-success"><?php echo $row['operation'];?></td> -->
          <td class="table-success"><?php echo $row['other_information'];?></td>
        </tr>
          <?php
              }
           }
           else {
             echo "No Record Found";
           }
          ?>

      </tbody>
    </table>
  </div>
</div>
</div>

  </div>



</div>


<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->




<?php
include('staff_includes/user_script.php');
include('staff_includes/user_footer.php');
 ?>
