<?php
session_start();
include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>



      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="../staff/img/staff.jpg">
            <h6>Staff</h6>
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> Add New Patient Record </h1>

      </div>



<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Patient Profile</h6>
</div>
<div class="card-body">


  <div class="table-responsive">
    <form align="center" action="../admin/code.php" method="POST">
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-6">
          <b><label for="inputAddress">  Name </label></b>
          <input type="text" name="name" class="form-control" id="inputAddress" placeholder="">
        </div>
        <div class="form-group col-md-4">
          <b><label for="inputAddress">  School ID </label></b>
          <input type="school_id" name="school_id" class="form-control" id="inputAddress" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputEmail4"> Phone </label></b>
          <input type="phone" name="phone" class="form-control" id="inputPhone" placeholder="Phone Number">
        </div>
        <div class="form-group col-md-4">
          <b><label for="inputStatus"> Description </label></b>
          <select id="Status" name="description"class="form-control">
            <option selected>Choose...</option>
            <option> Student </option>
            <option> Employee </option>
            <option> Instructor </option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <b><label for="inputStatus"> Gender </label></b>
          <select id="Status" name="gender" class="form-control">
            <option selected>Choose...</option>
            <option> Male </option>
            <option> Female </option>
            <option> Others </option>
          </select>
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputCity">Birthday</label></b>
          <input type="date" name="birthday" value="<?php echo date('Y-m-d'); ?>"s class="form-control" id="inputCity" placeholder="Mm-dd-Yy">
        </div>
        <div class="form-group col-md-4">
          <b><label for="inputStatus">Status</label></b>
          <select id="Status" name="status" class="form-control">
            <option selected>Choose...</option>
            <option>Active</option>
            <option>Inactive</option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <b><label for="Age">Age</label></b>
          <input type="age" name="age" class="form-control" id="inputZip" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputCity">City</label></b>
          <input type="text" name="city" class="form-control" id="inputCity" placeholder="">
        </div>
        <div class="form-group col-md-4">
          <b><label for="state"> Province </label></b>
          <select id="Status" name="province" class="form-control">
            <option selected>Choose...</option>
            <option>Misamis Oriental</option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <b><label for="Zip">Zip</label></b>
          <input type="zip" name="zip" class="form-control" id="inputZip" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-12">
          <b><label for="inputCity"> Address </label></b>
          <input type="text" name="address" class="form-control" id="inputCity" placeholder="">
        </div>
        <br>
        <div class="col-md-12 text-center">
                <button type="submit" name="savebtn" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-warning">Cancel</button>
        </div>
    </form>

  </div>
</div>
</div>

  </div>

</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> Medical history </h6>
</div>
<br>
<!-- Search form -->
<form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
<button type="submit" class="btn btn-primary"> Search</button>
<input class="form-control form-control-default mr-3 w-50" type="text" placeholder="Search"
  aria-label="Search">
<i class="fas fa-search" aria-hidden="true"></i>
</form>
<div class="card-body">


  <div class="table-responsive">
    <form align="center">
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-10">
          <b><label for="inputAddress"> Name </label></b>
          <input type="text" class="form-control" id="inputAddress" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputEmail4"> Phone </label></b>
          <input type="phone" class="form-control" id="inputPhone" placeholder="Phone Number">
        </div>
        <div class="form-group col-md-4">
          <b><label for="inputStatus"> Description </label></b>
          <select id="Status" class="form-control">
            <option selected>Choose...</option>
            <option> Student </option>
            <option> Employee </option>
            <option> Instructor </option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <label for="inputStatus"> Gender </label></b>
          <b><select id="Status" class="form-control">
            <option selected>Choose...</option>
            <option> Male </option>
            <option> Female </option>
            <option> Others </option>
          </select>
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputCity">Birthday</label></b>
          <input type="text" class="form-control" id="inputCity" placeholder="Mm-dd-Yy">
        </div>
        <div class="form-group col-md-4">
          <b><label for="inputStatus">Status</label></b>
          <select id="Status" class="form-control">
            <option selected>Choose...</option>
            <option>Active</option>
            <option>Inactive</option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <b><label for="Age">Age</label></b>
          <input type="age" class="form-control" id="inputZip" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputCity">City</label></b>
          <input type="text" class="form-control" id="inputCity" placeholder="">
        </div>
        <div class="form-group col-md-4">
          <b><label for="state"> Province </label></b>
          <select id="Status" class="form-control">
            <option selected>Choose...</option>
            <option>Misamis Oriental</option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <b><label for="Zip">Zip</label></b>
          <input type="zip" class="form-control" id="inputZip" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-12">
          <b><label for="inputCity"> Address </label></b>
          <input type="text" class="form-control" id="inputCity" placeholder="">
        </div>
        <br>
        <div class="col-md-12 text-center">
                <button type="button" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-warning">Cancel</button>
        </div>
    </form>

  </div>
</div>
</div>

  </div>

</div>
<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> Prescription </h6>
</div>
<br>
<!-- Search form -->
<form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
<button type="submit" class="btn btn-primary"> Search</button>
<input class="form-control form-control-default mr-3 w-50" type="text" placeholder="Search"
  aria-label="Search">
<i class="fas fa-search" aria-hidden="true"></i>
</form>
<div class="card-body">


  <div class="table-responsive">
    <form align="center">
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-10">
          <b><label for="inputAddress"> Name </label></b>
          <input type="text" class="form-control" id="inputAddress" placeholder="">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
          <b><label for="inputEmail4"> Symptoms </label></b>
          <input type="phone" class="form-control" id="inputPhone" placeholder="...">
        </div>
        <div class="form-group col-md-2">
          <b><label for="inputStatus"> Gender </label></b>
          <select id="Status" class="form-control">
            <option selected>...</option>
            <option> Male </option>
            <option> Female </option>
            <option> Others </option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <b><label for="inputEmail4"> Age </label></b>
          <input type="phone" class="form-control" id="inputPhone" placeholder="...">
        </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-6">
          <b><label for="exampleFormControlTextarea1"> Medications </label></b>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="form-group col-md-6">
          <b><label for="exampleFormControlTextarea1"> Instructions </label></b>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="col-md-12 text-center">
                <button type="button" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-warning">Cancel</button>
        </div>
      </div>
    </form>

</div>
</div>

  </div>

</div>
<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->


<?php
include('staff_includes/user_script.php');
include('staff_includes/user_footer.php');
 ?>
