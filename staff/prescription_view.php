<?php


include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
?>

      <div class="container-fluid">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>
              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>



                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="img-profile rounded-circle" src="img/staff.jpg">
                  </a>
                  <!-- Dropdown - User Information -->
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                      Settings
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                      Activity Log
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Logout
                    </a>
                  </div>
                </li>

              </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

              <!-- Page Heading -->
              <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System</h1>
              </div>

              <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Admin Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="code.php" method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label> Username </label>
                        <input type="text" name="username" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control checking_email" placeholder="Enter Email">
                        <small class="error_email" style="color: red;"></small>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
                </div>
              </form>

            </div>
          </div>
        </div>

        <div class="container-fluid">

        <!-- DataTables Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 front-weight-bold text primary">Edit Prescription</h6>
        </div>
        <div class="card-body">
<?php
$connection = mysqli_connect("localhost","root","","adminpanel");
if(isset($_POST['prescrition_view_btn']))
{
    $id = $_POST['prescrition_id'];

    $query = "SELECT * FROM doctor_prescription WHERE id='$id' ";
    $query_run = mysqli_query($connection, $query);

    foreach($query_run as $row)
  {
  ?>


  <form action="user_code.php" method="POST">
    <input type="hidden" name="prescription_id" value="<?php echo $row['id'] ?>">
    <div class="modal-body">
      <div class="form-row d-flex justify-content-center">
        <div class="form-group col-md-4">
            <label>  ID </label>
            <input type="prescriptionid" name="pres_id" value="<?php echo $row['school_id'] ?> "class="form-control" placeholder="">
        </div>
        <div class="form-group col-md-6">
            <label> Name : (Last - First - Middle) </label>
            <input type="text" name="pres_name" value="<?php echo $row['name'] ?> " class="form-control checking_email" placeholder="" required>
            <small class="error_email" style="color: red;"></small>
        </div>
        <div class="form-group col-md-2">
          <label for="inputGender">Gender</label>
          <select id="inputGender" class="form-control" type="text" name="pres_gender">
            <option selected><?php echo $row['gender'] ?></option>
            <option>Male</option>
            <option>Female</option>
          </select>
        </div>
        </div>
        <div class="form-row d-flex justify-content-center">

          <!-- <div class="form-group col-md-2">
              <label>Age</label>
              <input type="age" name="pres_age" value="<?php echo $row['age'] ?> "class="form-control" placeholder="">
          </div> -->
          <div class="form-group col-md-6">
              <label for="exampleFormControlTextarea1"> Symptom </label>
              <textarea class="form-control" type="text" name="pres_symptom" id="exampleFormControlTextarea1" rows="3"><?php echo $row['symptoms'] ?></textarea>
          </div>
          <div class="form-group col-md-6">
              <label for="exampleFormControlTextarea1"> Medication </label>
              <textarea class="form-control" name="pres_medication" id="exampleFormControlTextarea1" rows="3"><?php echo $row['medications'] ?></textarea>
          </div>
      </div>
      <div class="form-row d-flex justify-content-center">
        <!-- <div class="form-group col-md-6">
            <label for="exampleFormControlTextarea1"> Diagnosis </label>
            <textarea class="form-control" name="pres_diagnosis" id="exampleFormControlTextarea1" rows="3"><?php echo $row['diagnosis'] ?></textarea>
        </div> -->
        <div class="form-group col-md-10">
              <label for="exampleFormControlTextarea1"> Instruction </label>
              <textarea class="form-control" name="pres_instruction" id="exampleFormControlTextarea1" rows="3"><?php echo $row['instructions'] ?></textarea>
          </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="user_prescription.php" class="btn btn-secondary"> Close </a>
    </div>
  </form>

      <?php
    }
  }
      ?>
</div>
</div>
</div>
  <!-- /.container fluid-->


<?php
include('staff_includes/user_script.php');
include('staff_includes/user_footer.php');
 ?>
