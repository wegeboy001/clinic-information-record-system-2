<?php
session_start();

$connection = mysqli_connect("localhost","root","","adminpanel");

if(isset($_POST['savebtn']))
{
  $School_ID       = $_POST['patient_id'];
  $name            = $_POST['patient_name'];
  $Phone_No        = $_POST['patient_phone'];
  $Description     = $_POST['patient_description'];
  $Gender          = $_POST['patient_gender'];
  $Birthday        = $_POST['patient_birthday'];
  $Status          = $_POST['patient_status'];
  $Age             = $_POST['patient_age'];
  $City            = $_POST['patient_city'];
  $Province        = $_POST['patient_province'];
  $Zip             = $_POST['patient_zip'];
  $Address         = $_POST['patient_address'];

  $query = "INSERT INTO patient_record (school_id,name,phone,description,gender,date,status,age,city,province,zipcode,address)
  VALUES ('$School_ID','$name','$Phone_No', '$Description','$Gender','$Birthday','$Status','$Age','$City','$Province','$Zip','$Address')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: new_patient.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: new_patient.php');
  }
}



#Medical HISTORY

if(isset($_POST['medicalhistorybtn']))
{
  $School_ID           = $_POST['history_id'];
  $name                = $_POST['history_name'];
  $Drugallergies       = $_POST['history_allergies'];
  $Healthproblem       = $_POST['history_problem'];
  $Otherillness        = $_POST['history_illness'];
  $Operation           = $_POST['history_operation'];
  $Unhealthyhabbit     = $_POST['history_habbit'];
  $Medication          = $_POST['history_medication'];
  $Otherinformation    = $_POST['history_information'];

  $query = "INSERT INTO medical_history (school_id,name,drug_allergies,health_problem,other_illness,operation,unhealthy_habbits,medications,other_information)
  VALUES ('$School_ID','$name','$Drugallergies','$Healthproblem','$Otherillness','$Operation','$Unhealthyhabbit','$Medication','$Otherinformation')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: medical_history.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: medical_history.php');
  }
}



#Meidcal History Edit button
if(isset($_POST['medicalhistory_btn']))
{
  $id = $_POST['medicalhistory_id'];

  $query = "SELECT * FROM medical_history WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}



#Meidcal History Delete button
if(isset($_POST['medicalhistory_delete_btn']))
{
  $id = $_POST['medicalhistory_delete_id'];

  $query = "DELETE FROM medical_history WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted!";
    $_SESSION['status_code'] = "success";
    header('Location: medical_history.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Delete!";
    $_SESSION['status_code'] = "error";
    header('Location: medical_history.php');
  }
}



#Meidcal History Update button
if(isset($_POST['medical_history_update']))
{
  $id                 = $_POST['medicalhistory_id'];
  $history_schoolid   = $_POST['history_id'];
  $name                = $_POST['history_name'];
  $Drugallergies       = $_POST['history_allergies'];
  $Healthproblem       = $_POST['history_problem'];
  $Otherillness        = $_POST['history_illness'];
  $Operation            = $_POST['history_operation'];
  $habbit               = $_POST['history_habbit'];
  $medication         = $_POST['history_medication'];
  $information        = $_POST['history_information'];

  $query = "UPDATE medical_history SET school_id='$history_schoolid', name='$name', drug_allergies='$Drugallergies', health_problem='$Healthproblem', other_illness='$Otherillness',
  operation='$Operation', unhealthy_habbits='$habbit', medications='$medication', other_information='$information' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: medical_history.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: medical_history.php');
  }
}



#Presctiption Save button

if(isset($_POST['save_prescription']))
{
  $School_ID       = $_POST['id'];
  $name            = $_POST['name'];
  $gender          = $_POST['gender'];
  $age             = $_POST['age'];
  $symptoms        = $_POST['symptoms'];
  $diagnosis       = $_POST['diagnosis'];
  $Medication      = $_POST['medications'];
  $Instruction     = $_POST['instructions'];


  $query = "INSERT INTO doctor_prescription (school_id,name,gender,age,symptoms,diagnosis,medications,instructions)
  VALUES ('$School_ID','$name','$gender', '$age','$symptoms','$diagnosis','$Medication','$Instruction')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: user_prescription.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: user_prescription.php');
  }
}

#Prescription EDIT button
if(isset($_POST['prescription_edit']))
{
  $id = $_POST['prescription_id'];

  $query = "SELECT * FROM doctor_prescription WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}
#Prescription Delete button
if(isset($_POST['prescription_delete']))
{
  $id = $_POST['prescriptionid'];

  $query = "DELETE FROM doctor_prescription WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Deleted!";
    $_SESSION['status_code'] = "success";
    header('Location: user_prescription.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Delete!";
    $_SESSION['status_code'] = "error";
    header('Location: user_prescription.php');
  }
}

#Prescription UPDATE button
if(isset($_POST['prescription_update_btn']))
{
  $id             = $_POST['prescription_id'];
  $schoolid       = $_POST['pres_id'];
  $name           = $_POST['pres_name'];
  $gender         = $_POST['pres_gender'];
  $age            = $_POST['pres_age'];
  $symptoms       = $_POST['pres_symptom'];
  $diagnosis      = $_POST['pres_diagnosis'];
  $medication     = $_POST['pres_medication'];
  $instruction    = $_POST['pres_instruction'];


  $query = "UPDATE doctor_prescription SET school_id='$schoolid', name='$name', gender='$gender', age='$age', symptoms='$symptoms', diagnosis='$diagnosis',
  medications='$medication', instructions='$instruction' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: user_prescription.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: user_prescription.php');
  }
}


#New_patient Update button
if(isset($_POST['newpatient_update']))
{
  $id          = $_POST['newpatient_id'];
  $schoolid   = $_POST['patient_id'];
  $name        = $_POST['patient_name'];
  $phone       = $_POST['patient_phone'];
  $desc       = $_POST['patient_description'];
  $gender     = $_POST['[patient_gender]'];
  $bday       = $_POST['patient_birthday'];
  $status     = $_POST['patient_status'];
  $age        = $_POST['patient_age'];
  $city       = $_POST['patient_city'];
  $province   = $_POST['patient_province'];
  $zip        = $_POST['patient_zip'];
  $Address    = $_POST['patient_address'];

  $query = "UPDATE patient_record SET school_id='$schoolid', name='$name', phone='$phone', description='$desc', gender='$gender',
  date='$bday', status='$status', age='$age', city='$city', province='$province', zipcode='$zip', address='$Address' WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: new_patient.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: new_patient.php');
  }
}

#New Patient Edit button
if(isset($_POST['newpatient_edit_btn']))
{
  $id = $_POST['newpatient_id'];

  $query = "SELECT * FROM patient_record WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);
}
#DELETE button
if(isset($_POST['deletebtn']))
{
  $id = $_POST['delete_id'];

  $query = "DELETE FROM patient_record WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "The Data has been Deleted";
    $_SESSION['status_code'] = "success";
    header('Location: new_patient.php');
  }
  else
  {
    $_SESSION['status'] = "The Data is NOT Deleted";
    $_SESSION['status_code'] = "error";
    header('Location: new_patient.php');
  }
}

//Checkup Save button
if(isset($_POST['reg-checkup-btn']))
{

  $patient_id        = $_POST['patient_id'];
  // $name            = $_POST['name'];
  $medprac         = $_POST['mp_id'];
  $findings        = $_POST['findings'];
  $date            = $_POST['date'];
  $med_id          = $_POST['med_id'];
  $qty            = $_POST['qty'];
if ($qty > 0) {
  $nqty = $qty * -1;
  $query1 = "INSERT INTO medicine_transaction (patient_id,mp_id,date,medicine_id,qty,trans_type)
  VALUES ('$patient_id','$medprac','$date','$med_id','$nqty', 'RELEASED')";
  $query_run = mysqli_query($connection, $query1);
}

  $query = "INSERT INTO checkup (patient_id,mp_id,findings,date,medicine_id,qty)
  VALUES ('$patient_id','$medprac', '$findings','$date','$med_id','$qty')";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    //echo "Saved";
    $_SESSION['status'] = "Saved!";
    $_SESSION['status_code'] = "success";
    header('Location: checkup.php');
  }else
  {
    $_SESSION['status'] = "Failed to Save!";
    $_SESSION['status_code'] = "error";
    header('Location: checkup.php');
  }
}

#Checkup Edit button
if(isset($_POST['checkup-edit-btn']))
{
  $id = $_POST['checkup-id'];

  $query = "SELECT * FROM checkup WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

}

#Checkup Delete button
if(isset($_POST['checkup-delete']))
{
  $id = $_POST['delete-id'];

  $query = "DELETE FROM checkup WHERE id='$id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "The Data has been Deleted";
    $_SESSION['status_code'] = "success";
    header('Location: checkup.php');
  }
  else
  {
    $_SESSION['status'] = "The Data is NOT Deleted";
    $_SESSION['status_code'] = "error";
    header('Location: checkup.php');
  }
}

//Checkup update button
$oldqty = 0;
$newqty = 0;
$totalqty = 0;
if(isset($_POST['checkup-update-btn']))
{
  $checkup_id    = $_POST['checkup-id'];
  $patient_id    = $_POST['patient_id'];
  $mp_id         = $_POST['mp_id'];
  $findings      = $_POST['findings'];
  $medicine_id   = $_POST['medicine_id'];
  $date          = $_POST['date'];
  $medqty        = $_POST['qty'];


//Get old qty in order to minus the new updated quantity
  if ($medqty>0) {
    //GET OLD qty
    $sqlGetOldQty = "SELECT * FROM checkup WHERE id='$checkup_id' ";
    if ($resultGetOldQty = mysqli_query($connection,$sqlGetOldQty)) {
      if (mysqli_num_rows($resultGetOldQty) > 0) {
        while ($rowsGetOldQty = mysqli_fetch_array($resultGetOldQty)) {
          $oldqty= $rowsGetOldQty['qty'];
        }
      }
    }

    if ($oldqty>$medqty) {
      $newqty = $oldqty + $medqty;
      $queryInsertInventory = "INSERT INTO medicine_transaction (medicine_id,date,qty,patient_id,mp_id,trans_type) VALUES ('$medicine_id','$date','$newqty','$patient_id','$mp_id','CHECKUP EDIT')";
      mysqli_query($connection, $queryInsertInventory);

    }elseif ($oldqty<$medqty) {
      $newqty = $medqty-$oldqty;
      $totalqty = $newqty * -1;
      //$newqty = gmp_neg("1");
      $queryInsertInventory = "INSERT INTO medicine_transaction (medicine_id,date,qty,patient_id,mp_id,trans_type) VALUES ('$medicine_id','$date',$totalqty,'$patient_id','$mp_id','CHECKUP EDIT RELEASED')";
      mysqli_query($connection, $queryInsertInventory);
    }


  }

  //UPDATE CHECKUP
  $query = "UPDATE checkup SET patient_id='$patient_id', mp_id='$mp_id', findings='$findings', date='$date',medicine_id ='$medicine_id',qty='$medqty' WHERE id='$checkup_id' ";
  $query_run = mysqli_query($connection, $query);

  if($query_run)
  {
    $_SESSION['status'] = "Successfully Updated!";
    $_SESSION['status_code'] = "success";
    header('Location: checkup.php');
  }
  else
  {
    $_SESSION['status'] = "Failed to Update!";
    $_SESSION['status_code'] = "error";
    header('Location: checkup.php');
  }
}

//Medicine Invenetory

if(isset($_POST['add_medbtn']))
{
    $medid       = $_POST['medicine_number'];
    $medname     = $_POST['med_name'];
    $medstatus   = $_POST['med_status'];
    $medbrand    = $_POST['med_brand'];
    $meddesc     = $_POST['med_description'];
    $medprice    = $_POST['med_price'];



    $query = "INSERT INTO med_add (medicine_number,medicine_name,medicine_price,medicine_status,medicine_brand,medicine_desc)
    VALUES ('$medid','$medname', '$medprice','$medstatus','$medbrand', '$meddesc')";
    $query_run = mysqli_query($connection, $query);

    if($query_run)
    {
      //echo "Saved";
      $_SESSION['status'] = "Saved!";
      $_SESSION['status_code'] = "success";
      header('Location: staff_add_med.php');
    }else
    {
      $_SESSION['status'] = "Failed to Save!";
      $_SESSION['status_code'] = "error";
      header('Location: staff_add_med.php');
    }
  }


// Stock in
  if(isset($_POST['savebtn_medinventory']))
  {
      $medid       = $_POST['medicine_id'];
      $qty         = $_POST['qty'];
      $mp_id       = $_POST['mp_id'];
      $patient_id  = $_POST['patient_id'];
      $date        = $_POST['date'];



      $query = "INSERT INTO medicine_transaction (medicine_id,qty,mp_id,patient_id,date,trans_type)
      VALUES ('$medid','$qty', '$mp_id','$patient_id','$date', 'STOCK IN')";
      $query_run = mysqli_query($connection, $query);

      if($query_run)
      {
        //echo "Saved";
        $_SESSION['status'] = "Saved!";
        $_SESSION['status_code'] = "success";
        header('Location: staff_med_stockin.php');
      }else
      {
        $_SESSION['status'] = "Failed to Save!";
        $_SESSION['status_code'] = "error";
        header('Location: staff_med_stockin.php');
      }
    }


    // Stock Out
      if(isset($_POST['savebtn_medinventory_stockout']))
      {
          $medid       = $_POST['medicine_id'];
          $qty         = $_POST['qty'] * -1;
          $mp_id       = $_POST['mp_id'];
          $patient_id  = $_POST['patient_id'];
          $date        = $_POST['date'];



          $query = "INSERT INTO medicine_transaction (medicine_id,qty,mp_id,patient_id,date,trans_type)
          VALUES ('$medid','$qty', '$mp_id','$patient_id','$date', 'RELEASED')";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            //echo "Saved";
            $_SESSION['status'] = "Saved!";
            $_SESSION['status_code'] = "success";
            header('Location: med_inventory_stockout.php');
          }else
          {
            $_SESSION['status'] = "Failed to Save!";
            $_SESSION['status_code'] = "error";
            header('Location: med_inventory_stockout.php');
          }
        }


        //Add Medicine EDIT

        if(isset($_POST['edit_addmed_btn']))
        {
          $id = $_POST['medicine_id'];

          $query = "SELECT * FROM patient_record WHERE id='$id' ";
          $query_run = mysqli_query($connection, $query);
        }

        //Add Medicine DELETE

        if(isset($_POST['delete_medicine_btn']))
        {
          $id = $_POST['delete_med_id'];

          $query = "DELETE FROM med_add WHERE medicine_id='$id' ";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            $_SESSION['status'] = "Deleted Successfully";
            $_SESSION['status_code'] = "success";
            header('Location: staff_add_med.php');
          }
          else
          {
            $_SESSION['status'] = "Failed.!";
            $_SESSION['status_code'] = "error";
            header('Location: staff_add_med.php');
          }
        }

        //Stock In DELETE
        if(isset($_POST['edit_stockin_btn']))
        {
          $id = $_POST['stockin_id'];

          $query = "DELETE FROM medicine_transaction WHERE id='$id' ";
          $query_run = mysqli_query($connection, $query);

          if($query_run)
          {
            $_SESSION['status'] = "Deleted Successfully";
            $_SESSION['status_code'] = "success";
            header('Location: add_med.php');
          }
          else
          {
            $_SESSION['status'] = "Failed.!";
            $_SESSION['status_code'] = "error";
            header('Location: add_med.php');
          }
        }

        // Appointment Insert

        if(isset($_POST['add_appointment_btn']))
        {

            $patient_id       = $_POST['patient_id'];
            $app_date         = $_POST['app_date'];
            $app_time         = $_POST['app_time'];
            $mp_id            = $_POST['mp_id'];




            $query = "INSERT INTO appointment (patient_id,app_date,app_time,mp_id)
            VALUES ('$patient_id','$app_date', '$app_time','$mp_id')";
            $query_run = mysqli_query($connection, $query);

            if($query_run)
            {
              //echo "Saved";
              $_SESSION['status'] = "Saved!";
              $_SESSION['status_code'] = "success";
              header('Location: staff_appointment.php');
            }else
            {
              $_SESSION['status'] = "Failed to Save!";
              $_SESSION['status_code'] = "error";
              header('Location: staff_appointment.php');
            }
          }

          //Appointment DELETE

          if(isset($_POST['delete_app_btn']))
          {
            $id = $_POST['delete_app_id'];

            $query = "DELETE FROM appointment WHERE appointment_id='$id' ";
            $query_run = mysqli_query($connection, $query);

            if($query_run)
            {
              $_SESSION['status'] = "Deleted Successfully";
              $_SESSION['status_code'] = "success";
              header('Location: staff_appointment.php');
            }
            else
            {
              $_SESSION['status'] = "Failed.!";
              $_SESSION['status_code'] = "error";
              header('Location: staff_appointment.php');
            }
          }

          //appointment EDIT
          $appointment_id = 0;
          $patient_id = 0;
          $app_date ="";
          $app_time = "";
          $mp_id = 0;
          $patient_name = "";
          $mp_name = "";

          if(isset($_POST['appointment_update_btn']))
          {
            $id = $_POST['appointment_id'];


            $patient_id = $_POST['patient_id'];
            $app_date = $_POST['app_date'];
            $app_time = $_POST['app_time'];
            $mp_id = $_POST['mp_id'];

            $query = "UPDATE appointment SET patient_id='$patient_id', app_date='$app_date',app_time='$app_time',mp_id='$mp_id' WHERE appointment_id = $id ";
            $query_run = mysqli_query($connection, $query);

            if($query_run)
            {
              //echo "Saved";
              $_SESSION['status'] = "Saved!";
              $_SESSION['status_code'] = "success";
              header('Location: staff_appointment.php');
            }else
            {
              $_SESSION['status'] = "Failed to Save!";
              $_SESSION['status_code'] = "error";
              header('Location: staff_appointment.php');
            }
          }



?>
