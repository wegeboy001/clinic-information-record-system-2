<?php


include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
 ?>


 <!-- Content Wrapper -->
 <div id="content-wrapper" class="d-flex flex-column">

   <!-- Main Content -->
   <div id="content">

     <!-- Topbar -->
     <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

       <!-- Sidebar Toggle (Topbar) -->
       <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
         <i class="fa fa-bars"></i>
       </button>



       <!-- Topbar Navbar -->
       <ul class="navbar-nav ml-auto">

         <!-- Nav Item - Search Dropdown (Visible Only XS) -->
         <li class="nav-item dropdown no-arrow d-sm-none">
           <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <i class="fas fa-search fa-fw"></i>
           </a>
           <!-- Dropdown - Messages -->
           <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
             <form class="form-inline mr-auto w-100 navbar-search">
               <div class="input-group">
                 <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                 <div class="input-group-append">
                   <button class="btn btn-primary" type="button">
                     <i class="fas fa-search fa-sm"></i>
                   </button>
                 </div>
               </div>
             </form>
           </div>
         </li>



         <!-- Nav Item - User Information -->
         <li class="nav-item dropdown no-arrow">
           <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <img class="img-profile rounded-circle" src="../staff/img/staff.jpg">
             <h6>Patient</h6>
           </a>
           <!-- Dropdown - User Information -->
           <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
             <a class="dropdown-item" href="#">
               <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
               Profile
             </a>
             <a class="dropdown-item" href="#">
               <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
               Settings
             </a>
             <a class="dropdown-item" href="#">
               <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
               Activity Log
             </a>
             <div class="dropdown-divider"></div>
             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
               <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
               Logout
             </a>
           </div>
         </li>

       </ul>

     </nav>
     <!-- End of Topbar -->

     <!-- Begin Page Content -->
     <div class="container-fluid">

       <!-- Page Heading -->
       <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h3 mb-0 text-gray-800">Clinic Information Record System (Patient Dashboard)</h1>
       </div>

       <!-- Content Row -->
       <div class="row">

         <!-- Earnings (Monthly) Card Example -->

         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-success shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="new_patient.php">
                     <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Patient</div>
                   <div class="h5 mb-0 font-weight-bold text-gray-800"></div>

                   <!-- <?php
                   $query = "SELECT id FROM patient_record ORDER BY id";
                   $query_run = mysqli_query($connection, $query);

                   $row = mysqli_num_rows($query_run);

                   echo '<h4> Record: '.$row.'</h4>';
                   ?> -->
                   <h5> New Patient </h5>


                 </div>
                 <div class="col-auto">
                   <i class="fa fa-fw fa-user fa-2x text-gray-300"></i>
                 </div>
               </div></a>
             </div>
           </div>
         </div>

         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-warning shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="medical_history.php">
                     <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Medical History</div>
                   <div class="h5 mb-0 font-weight-bold text-gray-800"></div>

                   <!-- <?php
                   $query = "SELECT id FROM patient_record ORDER BY id";
                   $query_run = mysqli_query($connection, $query);

                   $row = mysqli_num_rows($query_run);

                   echo '<h4> Record: '.$row.'</h4>';
                   ?> -->
                   <h5> Add Medical History </h5>


                 </div></a>
                 <div class="col-auto">
                   <i class="fas fa-calendar fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>

         <!-- Earnings (Monthly) Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-primary shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <a class="nav-link" href="user_prescription.php">
                     <div class="text-xs font-weight-bold text-success text-uppercase mb-1">prescription</div>
                   <div class="h5 mb-0 font-weight-bold text-gray-800"></div>

                   <!-- <?php
                   $query = "SELECT id FROM patient_record ORDER BY id";
                   $query_run = mysqli_query($connection, $query);

                   $row = mysqli_num_rows($query_run);

                   echo '<h4> Record: '.$row.'</h4>';
                   ?> -->
                   <h5> Prescriptions </h5>


                 </div></a>
                 <div class="col-auto">
                   <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>

         <!-- Pending Requests Card Example -->
         <div class="col-xl-3 col-md-6 mb-4">
           <div class="card border-left-info shadow h-100 py-2">
             <div class="card-body">
               <div class="row no-gutters align-items-center">
                 <div class="col mr-2">
                   <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Medicine </div>
                   <div class="h5 mb-0 font-weight-bold text-gray-800"></div>

                   <!-- <?php
                   $query = "SELECT * FROM patient_record ORDER BY id";
                   $query_run = mysqli_query($connection, $query);

                   $row = mysqli_num_rows($query_run);

                   echo '<h4> Record: '.$row.'</h4>';
                   ?> -->


                 </div>
                 <div class="col-auto">
                   <i class="fas fa-file-prescription fa-2x text-gray-300"></i>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>

       <!-- Content Row -->
       <div class="container-fluid">

       <!-- DataTables Example -->
       <div class="card-body text-center">
         <img src="../staff/img/clinic-wallpaper.jpg" class="img-fluid" alt="Responsive image">
       </div>
       </div>

         </div>

      </div>
      <!-- End of Main Content -->



<?php
include('staff_includes/user_script.php');
include('staff_includes/user_footer.php');

?>
