<?php

include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
?>

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

      <!-- Sidebar Toggle (Topbar) -->
      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
      </button>

      <!-- Topbar Search -->


      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
          <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-search fa-fw"></i>
          </a>
          <!-- Dropdown - Messages -->
          <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
              <div class="input-group">
                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>



        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="img-profile rounded-circle" src="../staff/img/staff.jpg">
            <h6>Staff</h6>

          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Profile
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Settings
            </a>
            <a class="dropdown-item" href="#">
              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
              Activity Log
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Logout
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">  Medicine Inventory </h1>

      </div>

      <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Update Inventory</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="code.php" method="POST">
        <div class="modal-body">

            <div class="form-group">
                <label> Order Code </label>
                <input type="text" name="order" class="form-control" placeholder="Enter code">
            </div>
            <div class="form-group">
                <label> Suppier </label>
                <input type="email" name="Supplier" class="form-control checking_email" placeholder="Enter supplier">
                <small class="error_email" style="color: red;"></small>
            </div>
            <div class="form-group">
                <label> Quantity </label>
                <input type="password" name="quantity" class="form-control" placeholder="Enter quantity">
            </div>
            <div class="form-group">
                <label> Order Date </label>
                <input type="password" name="order_date" class="form-control" placeholder="Enter date">
            </div>
                <div>
                  <label> Status </label>
                  <select class="form-control">
                  <option> Available</option>
                  <option> Not Available</option>
                  </select>
                </div>

          </form>

            <input type="hidden" name="usertype" value="admin">


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="container-fluid">

<!-- DataTables Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"> Stock </h6>
    <br>
    <!-- Search form -->
  <form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
    <button type="submit" class="btn btn-primary"> Search</button>
    <input class="form-control form-control-default mr-3 w-50" type="text" placeholder="Search"
      aria-label="Search">
    <i class="fas fa-search" aria-hidden="true"></i>
  </form>
  <br>
    <div class="form-row d-flex justify-content-center">
      <div class="form-group col-md-2">
        <b><label for="inputStatus">  Type </label></b>
        <select id="Status" class="form-control">
          <option selected>...</option>
          <option> Liquid </option>
          <option> Tablet </option>
          <option> Capsules </option>
          <option> Drops </option>
          <option> Injectables </option>
        </select>
      </div>
      <div class="form-group col-md-2">
        <b><label for="inputStatus"> Category </label></b>
        <select id="Status" class="form-control">
          <option selected>...</option>
          <option> Antibiotic </option>
          <option> Antipyretics </option>
          <option> Antiseptics </option>
          <option> Stimulants </option>
          <option> Anesthetics </option>
        </select>
      </div>
      <div class="form-group col-md-2">
        <b><label for="inputStatus"> Status </label></b>
        <select id="Status" class="form-control">
          <option selected>...</option>
          <option> Available </option>
          <option> Not Available </option>
        </select>
      </div>
      <div class="form-group col-md-2">
        <b><label for="inputPassword4"> Quantity </label></b>
        <input type="lastname" class="form-control" id="inputLasname" placeholder="">
      </div>
    </div>
    <br>
    <div class="col-md-12 text-center">
            <button type="button" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger">Cancel </button>
    </div>
    <div class="form-group">
    <b><label for="exampleFormControlSelect2">Result</label></b>
    <select multiple class="form-control" id="exampleFormControlSelect2">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  </div>
<div class="card-body">


  <?php
    if(isset($_SESSION['success']) && $_SESSION['success'] !='')
    {
      echo '<h2 class="bg-primary text-white"> '.$_SESSION['success'].' </h2>';
      unset($_SESSION['success']);
    }
    if(isset($_SESSION['status']) && $_SESSION['status'] !='')
    {
      echo '<h2 class="bg-danger text-white"> '.$_SESSION['status'].' </h2>';
      unset($_SESSION['status']);
    }
   ?>
  <div class="table-responsive">

    <?php
      $connection = mysqli_connect("localhost","root","","adminpanel");

      $query = "SELECT * FROM medicine_inventory";
      $query_run = mysqli_query($connection, $query);
     ?>
    <table class="table table-bordered table table-hover" id="dataTable" width="100%" cellspacing="0">
      <thead>
        <tr class="table-danger">
          <th> ID</th>
          <th> name</th>
          <th>Description</th>
          <th>Order date</th>
          <th> code</th>
          <th>Supplier</th>
          <th>Category</th>
          <th>Status</th>
          <th>Quantity</th>
          <th>View</th>
        </tr>
      </thead>
      <tbody>

          <?php
            if(mysqli_num_rows($query_run) > 0)
            {
                while ($row = mysqli_fetch_assoc($query_run))
                 {
                   ?>
        <tr>
          <td class="table-dark"><?php echo $row['id'];?></td>
          <td class="table-success"><?php echo $row['medicine_name'];?></td>
          <td class="table-success"><?php echo $row['description'];?></td>
          <td class="table-success"><?php echo $row['order_date'];?></td>
          <td class="table-success"><?php echo $row['order_code'];?></td>
          <td class="table-success"><?php echo $row['supplier'];?></td>
          <td class="table-success"><?php echo $row['category'];?></td>
          <td class="table-success"><?php echo $row['status'];?></td>
          <td class="table-success"><?php echo $row['quantity'];?></td>
          <td class="table-success">
              <form action="register_edit.php" method="post">
                  <input type="hidden" name="edit_id" value="<?php echo $row['id'];?>">
              <button type="submit" name="edit_btn"class="btn btn-success btn-sm">View</button>
              </form>
          </td>
        </tr>
          <?php
              }
           }
           else {
             echo "No Record Found";
           }
          ?>

      </tbody>
    </table>
  </div>
</div>
</div>

  </div>

</div>
<!-- /.container fluid-->


  </div>
  <!-- End of Main Content -->


<?php
include('staff_includes/user_script.php');
include('staff_includes/user_footer.php');
 ?>
