<?php


include('staff_includes/user_header.php');
include('staff_includes/user_navbar.php');
 ?>


 <!-- Content Wrapper -->
 <div id="content-wrapper" class="d-flex flex-column">

   <!-- Main Content -->
   <div id="content">

     <!-- Topbar -->
     <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

       <!-- Sidebar Toggle (Topbar) -->
       <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
         <i class="fa fa-bars"></i>
       </button>

       <!-- Topbar Navbar -->
       <ul class="navbar-nav ml-auto">

         <!-- Nav Item - Search Dropdown (Visible Only XS) -->
         <li class="nav-item dropdown no-arrow d-sm-none">
           <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <i class="fas fa-search fa-fw"></i>
           </a>
           <!-- Dropdown - Messages -->
           <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
             <form class="form-inline mr-auto w-100 navbar-search">
               <div class="input-group">
                 <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                 <div class="input-group-append">
                   <button class="btn btn-primary" type="button">
                     <i class="fas fa-search fa-sm"></i>
                   </button>
                 </div>
               </div>
             </form>
           </div>
         </li>



         <!-- Nav Item - User Information -->
         <li class="nav-item dropdown no-arrow">
           <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <img class="img-profile rounded-circle" src="../staff/img/staff.jpg">
             <h6>Staff</h6>
           </a>
           <!-- Dropdown - User Information -->
           <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
             <a class="dropdown-item" href="#">
               <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
               Profile
             </a>
             <a class="dropdown-item" href="#">
               <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
               Settings
             </a>
             <a class="dropdown-item" href="#">
               <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
               Activity Log
             </a>
             <div class="dropdown-divider"></div>
             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
               <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
               Logout
             </a>
           </div>
         </li>

       </ul>

     </nav>
     <!-- End of Topbar -->

     <!-- Begin Page Content -->
     <div class="container-fluid">

       <!-- Page Heading -->
       <div class="d-sm-flex align-items-center justify-content-between mb-4">
         <h1 class="h3 mb-0 text-gray-800"> Patient Medical Information </h1>
       </div>

         <div class="container-fluid">

         <!-- DataTables Example -->
         <div class="card shadow mb-4">
           <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary"> Patient Information </h6>
             <br>
             <!-- Search form -->
           <form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
             <button type="submit" class="btn btn-primary"> Search</button>
             <input class="form-control form-control-default mr-3 w-50" type="text" placeholder="Search"
               aria-label="Search">
             <i class="fas fa-search" aria-hidden="true"></i>
           </form>
           <br>
           <div class="col-md-12 text-center">
                   <button type="button" class="btn btn-primary">Previous</button>
                   <button type="button" class="btn btn-success">Next >></button>
           </div>
         </div>
         <div class="card-body">

           <?php
             if(isset($_SESSION['success']) && $_SESSION['success'] !='')
             {
               echo '<h2 class="bg-primary text-white"> '.$_SESSION['success'].' </h2>';
               unset($_SESSION['success']);
             }
             if(isset($_SESSION['status']) && $_SESSION['status'] !='')
             {
               echo '<h2 class="bg-danger text-white"> '.$_SESSION['status'].' </h2>';
               unset($_SESSION['status']);
             }
            ?>
           <div class="table-responsive">

             <?php
               $connection = mysqli_connect("localhost","root","","adminpanel");

               $query = "SELECT * FROM patient_record";
               $query_run = mysqli_query($connection, $query);
              ?>
             <table border="3" bordercolor="grey" class="table table-hover" id="dataTable" width="100%" cellspacing="10">
               <thead>
                 <tr class="table-danger">
                   <th> ID</th>
                   <th>Name</th>
                   <th>Phone No.</th>
                   <th>Description</th>
                   <th>Gender</th>
                   <th>Birthday</th>
                   <th>Status</th>
                   <th>Age</th>
                   <th>City</th>
                   <th>Province</th>
                   <th>Zip</th>
                   <th>Address</th>
                   <th>View</th>
                 </tr>
               </thead>
               <tbody>

                   <?php
                     if(mysqli_num_rows($query_run) > 0)
                     {
                         while ($row = mysqli_fetch_assoc($query_run))
                          {
                            ?>
                 <tr>
                   <td class="table-dark"><?php echo $row['id'];?></td>
                   <td class="table-success"><?php echo $row['name'];?></td>
                   <td class="table-success"><?php echo $row['phone'];?></td>
                   <td class="table-success"><?php echo $row['description'];?></td>
                   <td class="table-success"><?php echo $row['gender'];?></td>
                   <td class="table-success"><?php echo $row['birthday'];?></td>
                   <td class="table-success"><?php echo $row['status'];?></td>
                   <td class="table-success"><?php echo $row['age'];?></td>
                   <td class="table-success"><?php echo $row['city'];?></td>
                   <td class="table-success"><?php echo $row['province'];?></td>
                   <td class="table-success"><?php echo $row['zipcode'];?></td>
                   <td class="table-success"><?php echo $row['address'];?></td>
                   <td class="table-success">
                       <form action="register_edit.php" method="post">
                           <input type="hidden" name="edit_id" value="<?php echo $row['id'];?>">
                       <button type="submit" name="edit_btn"class="btn btn-success btn-sm"> View </button>
                       </form>
                   </td>
                 </tr>
                   <?php
                       }
                    }
                    else {
                      echo "No Record Found";
                    }
                   ?>

               </tbody>
             </table>
           </div>
         </div>
         </div>
         <!-- DataTables Example -->
         <div class="card shadow mb-4">
           <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary"> Medical History </h6>
             <!-- Search form -->
           <form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
             <button type="submit" class="btn btn-primary"> Search</button>
             <input class="form-control form-control-default mr-3 w-50" type="text" placeholder="Search"
               aria-label="Search">
             <i class="fas fa-search" aria-hidden="true"></i>
           </form>
           <br>
           <div class="col-md-12 text-center">
                   <button type="button" class="btn btn-primary">Previous</button>
                   <button type="button" class="btn btn-success">Next >></button>
           </div>
         </div>
         <div class="card-body">

           <?php
             if(isset($_SESSION['success']) && $_SESSION['success'] !='')
             {
               echo '<h2 class="bg-primary text-white"> '.$_SESSION['success'].' </h2>';
               unset($_SESSION['success']);
             }
             if(isset($_SESSION['status']) && $_SESSION['status'] !='')
             {
               echo '<h2 class="bg-danger text-white"> '.$_SESSION['status'].' </h2>';
               unset($_SESSION['status']);
             }
            ?>
           <div class="table-responsive">

             <?php
               $connection = mysqli_connect("localhost","root","","adminpanel");

               $query = "SELECT * FROM medical_history";
               $query_run = mysqli_query($connection, $query);
              ?>
             <table border="3" bordercolor="grey" class="table table-hover" id="dataTable" width="100%" cellspacing="10">
               <thead>
                 <tr class="table-danger">
                   <th> ID</th>
                   <th>Name</th>
                   <th>Drug allergy</th>
                   <th>Health problem</th>
                   <th>Other illness</th>
                   <th>Operation</th>
                   <th>Current medication</th>
                   <th>Unhealthy habbits</th>
                   <th>Other information</th>
                   <th>View</th>
                 </tr>
               </thead>
               <tbody>

                   <?php
                     if(mysqli_num_rows($query_run) > 0)
                     {
                         while ($row = mysqli_fetch_assoc($query_run))
                          {
                            ?>
                 <tr>
                   <td class="table-dark"><?php echo $row['id'];?></td>
                   <td class="table-success"><?php echo $row['name'];?></td>
                   <td class="table-success"><?php echo $row['drug_allergies'];?></td>
                   <td class="table-success"><?php echo $row['health_problem'];?></td>
                   <td class="table-success"><?php echo $row['other_illness'];?></td>
                   <td class="table-success"><?php echo $row['operation'];?></td>
                   <td class="table-success"><?php echo $row['unhealthy_habbits'];?></td>
                   <td class="table-success"><?php echo $row['other_information'];?></td>
                   <td class="table-success"><?php echo $row['other_information'];?></td>
                   <td class="table-success">
                       <form action="register_edit.php" method="post">
                           <input type="hidden" name="edit_id" value="<?php echo $row['id'];?>">
                       <button type="submit" name="edit_btn"class="btn btn-success btn-sm">View</button>
                       </form>
                   </td>
                 </tr>
                   <?php
                       }
                    }
                    else {
                      echo "No Record Found";
                    }
                   ?>

               </tbody>
             </table>
           </div>
         </div>
         </div>
         <!-- DataTables Example -->
         <div class="card shadow mb-4">
           <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary"> Prescription </h6>
             <!-- Search form -->
           <form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
             <button type="submit" class="btn btn-primary"> Search</button>
             <input class="form-control form-control-default mr-3 w-50" type="text" placeholder="Search"
               aria-label="Search">
             <i class="fas fa-search" aria-hidden="true"></i>
           </form>
           <br>
           <div class="col-md-12 text-center">
                   <button type="button" class="btn btn-primary">Previous</button>
                   <button type="button" class="btn btn-success">Next >></button>
           </div>
         </div>
         <div class="card-body">

           <?php
             if(isset($_SESSION['success']) && $_SESSION['success'] !='')
             {
               echo '<h2 class="bg-primary text-white"> '.$_SESSION['success'].' </h2>';
               unset($_SESSION['success']);
             }
             if(isset($_SESSION['status']) && $_SESSION['status'] !='')
             {
               echo '<h2 class="bg-danger text-white"> '.$_SESSION['status'].' </h2>';
               unset($_SESSION['status']);
             }
            ?>
           <div class="table-responsive">

             <?php
               $connection = mysqli_connect("localhost","root","","adminpanel");

               $query = "SELECT * FROM doctor_prescription";
               $query_run = mysqli_query($connection, $query);
              ?>
             <table border="3" bordercolor="grey" class="table table-hover" id="dataTable" width="100%" cellspacing="10">
               <thead>
                 <tr class="table-danger">
                   <th> ID</th>
                   <th>Name</th>
                   <th>Gender</th>
                   <th>Age</th>
                   <th>Symptoms</th>
                   <th>Diagnosis</th>
                   <th>Medications</th>
                   <th>Instructions</th>
                   <th>Date</th>
                   <th>View</th>
                 </tr>
               </thead>
               <tbody>

                   <?php
                     if(mysqli_num_rows($query_run) > 0)
                     {
                         while ($row = mysqli_fetch_assoc($query_run))
                          {
                            ?>
                 <tr>
                   <td class="table-dark"><?php echo $row['id'];?></td>
                   <td class="table-success"><?php echo $row['name'];?></td>
                   <td class="table-success"><?php echo $row['gender'];?></td>
                   <td class="table-success"><?php echo $row['age'];?></td>
                   <td class="table-success"><?php echo $row['symptoms'];?></td>
                   <td class="table-success"><?php echo $row['diagnosis'];?></td>
                   <td class="table-success"><?php echo $row['medications'];?></td>
                   <td class="table-success"><?php echo $row['instructions'];?></td>
                   <td class="table-success"><?php echo $row['date'];?></td>
                   <td class="table-success">
                       <form action="register_edit.php" method="post">
                           <input type="hidden" name="edit_id" value="<?php echo $row['id'];?>">
                       <button type="submit" name="edit_btn"class="btn btn-success btn-sm">View</button>
                       </form>
                   </td>
                 </tr>
                   <?php
                       }
                    }
                    else {
                      echo "No Record Found";
                    }
                   ?>

               </tbody>
             </table>
           </div>
         </div>
         </div>

           </div>
      <!-- End of Main Content -->



<?php
include('staff_includes/user_script.php');
include('staff_includes/user_footer.php');

?>
